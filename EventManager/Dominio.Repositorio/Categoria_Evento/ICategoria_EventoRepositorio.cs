﻿using Repositorio.Base;

namespace Dominio.Repositorio.Categoria_Evento
{
    public interface ICategoria_EventoRepositorio : IRepositorio<Entidades.Categoria_Evento>
    {

    }
}
