﻿using Repositorio.Base;

namespace Dominio.Repositorio.Resumen
{
    public interface IResumenRepositorio : IRepositorio<Entidades.Resumen>
    {

    }
}
