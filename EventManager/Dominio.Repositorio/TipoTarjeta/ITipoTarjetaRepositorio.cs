﻿using Repositorio.Base;

namespace Dominio.Repositorio.TipoTarjeta
{
    public interface ITipoTarjetaRepositorio : IRepositorio<Entidades.TipoTarjeta>
    {
    }
}
