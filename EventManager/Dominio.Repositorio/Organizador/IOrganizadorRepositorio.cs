﻿using Repositorio.Base;

namespace Dominio.Repositorio.Organizador
{
    public interface IOrganizadorRepositorio : IRepositorio<Entidades.Organizador>
    {

    }
}
