﻿using Repositorio.Base;

namespace Dominio.Repositorio.ListaBancos
{
    public interface IListaBancosRepositorio : IRepositorio<Entidades.ListaBancos>
    {
    }
}
