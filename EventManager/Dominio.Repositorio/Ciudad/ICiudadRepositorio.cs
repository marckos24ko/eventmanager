﻿using Repositorio.Base;

namespace Dominio.Repositorio.Ciudad
{
    public interface ICiudadRepositorio : IRepositorio<Entidades.Ciudad>
    {

    }
}
