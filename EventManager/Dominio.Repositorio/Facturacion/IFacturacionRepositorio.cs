﻿using Repositorio.Base;

namespace Dominio.Repositorio.Facturacion
{
    public interface IFacturacionRepositorio : IRepositorio<Entidades.Facturacion>
    {

    }
}
