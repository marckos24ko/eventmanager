﻿using Repositorio.Base;

namespace Dominio.Repositorio.Oyente
{
    public interface IOyenteRepositorio : IRepositorio<Entidades.Oyente>
    {

    }
}
