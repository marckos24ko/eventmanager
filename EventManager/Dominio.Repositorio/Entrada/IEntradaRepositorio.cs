﻿using Repositorio.Base;

namespace Dominio.Repositorio.Entrada
{
    public interface IEntradaRepositorio : IRepositorio<Entidades.Entrada>
    {

    }
}
