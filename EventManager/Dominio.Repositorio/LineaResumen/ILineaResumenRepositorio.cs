﻿using Repositorio.Base;

namespace Dominio.Repositorio.LineaResumen
{
    public interface ILineaResumenRepositorio : IRepositorio<Entidades.LineaResumen>
    {

    }
}
