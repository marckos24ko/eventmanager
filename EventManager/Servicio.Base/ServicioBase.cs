﻿using Infraestructura;

namespace Servicio.Base
{
    public class ServicioBase : IServicioBase
    {
        protected readonly EventManagerContext Context;

        public ServicioBase()
        {
            Context = new EventManagerContext(); ;
        }
    }
}
