﻿using System;

namespace Servicio.Base.Dtos
{
    public class DtoBase
    {
        public long Id { get; set; }

        public Byte[] RowVersion { get; set; }
    }
}
