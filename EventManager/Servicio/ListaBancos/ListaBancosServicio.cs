﻿using Dominio.Repositorio.ListaBancos;
using IServicio.ListaBancos;
using IServicio.ListaBancos.Dto;
using Servicio.Base;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.ListaBancos
{
    public class ListaBancosServicio : ServicioBase, IListaBancosServicio
    {
        private readonly IListaBancosRepositorio _listaBancosRepositorio;

        public ListaBancosServicio(IListaBancosRepositorio listaBancosRepositorio)
        {
            _listaBancosRepositorio = listaBancosRepositorio;
        }

        public IEnumerable<ListaBancosDto> GetAll()
        {
            var bancos = _listaBancosRepositorio.ObtenerTodo();

            return bancos.Select(x => new ListaBancosDto()
            {
                Id = x.Id,
                Nombre = x.Nombre
            });

        }
    }
}
