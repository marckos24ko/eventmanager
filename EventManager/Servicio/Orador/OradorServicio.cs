﻿using Dominio.Repositorio.Orador;
using IServicio.Orador;
using IServicio.Orador.Dto;
using Servicio.Base;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.Orador
{
    public class OradorServicio : ServicioBase, IOradorServicio
    {
        private readonly IOradorRepositorio _oradorRepositorio;

        public OradorServicio(IOradorRepositorio oradorRepositorio)
        {
            _oradorRepositorio = oradorRepositorio;
        }

        public void Add(OradorDto Dto)
        {
            var orador = new Dominio.Entidades.Oradors()
            {

                Apellido = Dto.Apellido,
                Nombre = Dto.Nombre,
                Celular = Dto.Celular,
                Cuil = Dto.Cuil,
                FechaNacimiento = Dto.FechaNacimiento,
                Genero = Dto.Genero,
                Telefono = Dto.Telefono,
                Estado = true,
                CiudadId = Dto.CiudadId,
                ProvinciaId = Dto.ProvinciaId,
                RowVersion = Dto.RowVersion

            };

            _oradorRepositorio.Agregar(orador);
            _oradorRepositorio.Save();
        }

        public void Delete(long Id)
        {
            //si el sistema se rompe, agregar var oyente = _oyenteRepositorio.ObtenerPorId(Id, "Provincia, Ciudad");
            _oradorRepositorio.Eliminar(Id);
            _oradorRepositorio.Save();
        }

        public IEnumerable<OradorDto> GetByFilter(string cadenaBuscar)
        {
            return _oradorRepositorio.ObtenerPorFlitro(x => x.Apellido.Contains(cadenaBuscar), "Provincia, Ciudad")
                .Select(x => new OradorDto()
                {
                    Id = x.Id,
                    Apellido = x.Apellido,
                    Nombre = x.Nombre,
                    Celular = x.Celular,
                    Cuil = x.Cuil,
                    FechaNacimiento = x.FechaNacimiento,
                    Genero = x.Genero,
                    Telefono = x.Telefono,
                    Estado = x.Estado,
                    CiudadStr = x.Ciudad.Nombre,
                    ProvinciaStr = x.Provincia.Nombre,
                    ProvinciaId = x.ProvinciaId,
                    CiudadId = x.CiudadId

                }).ToList();
        }

        public OradorDto GetById(long? Id)
        {
            var orador = _oradorRepositorio.ObtenerPorId(Id, "Provincia, Ciudad");

            return new OradorDto()
            {
                Id = orador.Id,
                Apellido = orador.Apellido,
                Nombre = orador.Nombre,
                Celular = orador.Celular,
                Cuil = orador.Cuil,
                FechaNacimiento = orador.FechaNacimiento,
                Genero = orador.Genero,
                Telefono = orador.Telefono,
                Estado = orador.Estado,
                CiudadId = orador.CiudadId,
                CiudadStr = orador.Ciudad.Nombre,
                ProvinciaId = orador.ProvinciaId,
                ProvinciaStr = orador.Provincia.Nombre
            };
        }

        public void Update(OradorDto Dto)
        {
            var orador = _oradorRepositorio.ObtenerPorId(Dto.Id, "Provincia, Ciudad");

            orador.Apellido = Dto.Apellido;
            orador.Nombre = Dto.Nombre;
            orador.Celular = Dto.Celular;
            orador.Cuil = Dto.Cuil;
            orador.FechaNacimiento = Dto.FechaNacimiento;
            orador.Genero = Dto.Genero;
            orador.Telefono = Dto.Telefono;
            orador.Estado = Dto.Estado;
            orador.CiudadId = Dto.CiudadId;
            orador.ProvinciaId = Dto.ProvinciaId;

            _oradorRepositorio.Modificar(orador);
            _oradorRepositorio.Save();
        }
    }
}
