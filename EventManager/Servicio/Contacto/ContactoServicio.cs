﻿using Dominio.Repositorio.Contacto;
using IServicio.Contacto;
using IServicio.Contacto.Dto;
using Servicio.Base;
using System;
using System.Collections.Generic;

namespace Servicio.Contacto
{
    public class ContactoServicio : ServicioBase, IContactoServicio
    {
        private readonly IContactoRepositorio _contactoRepositorio;

        public ContactoServicio(IContactoRepositorio contactoRepositorio)
        {
            _contactoRepositorio = contactoRepositorio;
        }

        public void Add(ContactoDto Dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(long Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ContactoDto> GetByFilter(string cadenaBuscar)
        {
            throw new NotImplementedException();
        }

        public ContactoDto GetById(long? Id)
        {
            throw new NotImplementedException();
        }

        public void Update(ContactoDto Dto)
        {
            throw new NotImplementedException();
        }
    }
}
