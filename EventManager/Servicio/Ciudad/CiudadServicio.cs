﻿using Dominio.Repositorio.Ciudad;
using IServicio.Ciudad;
using IServicio.Ciudad.Dto;
using Servicio.Base;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.Ciudad
{
    public class CiudadServicio : ServicioBase, ICiudadServicio
    {
        private readonly ICiudadRepositorio _ciudadRepositorio;

        public CiudadServicio(ICiudadRepositorio ciudadRepositorio)
        {
            _ciudadRepositorio = ciudadRepositorio;
        }

        public void Add(CiudadDto Dto)
        {
            var ciudad = new Dominio.Entidades.Ciudad()
            {
                Nombre = Dto.Nombre,
                CP = Dto.CP,
                ProvinciaId = Dto.ProvinciaId,
                RowVersion = Dto.RowVersion
            };

            _ciudadRepositorio.Agregar(ciudad);
            _ciudadRepositorio.Save();
        }

        public void Delete(long Id)
        {
            _ciudadRepositorio.Eliminar(Id);
            _ciudadRepositorio.Save();

        }

        public IEnumerable<CiudadDto> GetByFilter(string cadenaBuscar)
        {
            return _ciudadRepositorio.ObtenerPorFlitro(x => x.Nombre.Contains(cadenaBuscar), "Provincia")
                .Select(x => new CiudadDto()
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    CP = x.CP,
                    ProvinciaId = x.ProvinciaId,
                    ProvinciaStr = x.Provincia.Nombre
                }).ToList();


        }

        public CiudadDto GetById(long? Id)
        {
            var ciudad = _ciudadRepositorio.ObtenerPorId(Id, "Provincia");

            return new CiudadDto()
            {
                Id = ciudad.Id,
                Nombre = ciudad.Nombre,
                CP = ciudad.CP,
                ProvinciaId = ciudad.ProvinciaId,
                ProvinciaStr = ciudad.Provincia.Nombre
            };
        }

        public void Update(CiudadDto Dto)
        {
            var ciudad = _ciudadRepositorio.ObtenerPorId(Dto.Id, "Provincia");

            ciudad.Nombre = Dto.Nombre;
            ciudad.CP = Dto.CP;
            ciudad.ProvinciaId = Dto.ProvinciaId;

            _ciudadRepositorio.Modificar(ciudad);
            _ciudadRepositorio.Save();

        }

        public IEnumerable<CiudadDto> GetAll()
        {
            return _ciudadRepositorio.ObtenerTodo("Provincia")
                            .Select(x => new CiudadDto()
                            {
                                Id = x.Id,
                                Nombre = x.Nombre,
                                CP = x.CP,
                                ProvinciaId = x.ProvinciaId,
                                ProvinciaStr = x.Provincia.Nombre,

                            }).ToList();
        }

        public IEnumerable<CiudadDto> GetAll(long id)
        {
            var ciudades = _ciudadRepositorio.ObtenerTodo("Provincia")
                            .Where(x => x.ProvinciaId == id)
                            .Select(x => new CiudadDto()
                            {
                                Id = x.Id,
                                Nombre = x.Nombre
                            }).ToList();
            return ciudades;
        }

    }
}
