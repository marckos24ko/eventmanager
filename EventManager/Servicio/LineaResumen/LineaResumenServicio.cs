﻿using Dominio.Repositorio.LineaResumen;
using IServicio.LineaResumen;
using IServicio.LineaResumen.Dto;
using Servicio.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.LineaResumen
{
    public class LineaResumenServicio : ServicioBase, ILineaResumenServicio
    {
        private readonly ILineaResumenRepositorio _lineaResumenRepositorio;

        public LineaResumenServicio(ILineaResumenRepositorio lineaResumenRepositorio)
        {
            _lineaResumenRepositorio = lineaResumenRepositorio;
        }

        public void Add(LineaResumenDto Dto)
        {
            var LineaResumen = new Dominio.Entidades.LineaResumen()
            {
                Id = ObtenerSiguienteId(),
                Cantidad = Dto.Cantidad,
                Precio = Dto.Precio,
                EntradaId = Dto.EntradaId,
                ResumenId = Dto.ResumenId,
                TipoEntrada = Dto.TipoEntrada,
                Subtotal = Dto.Precio * Dto.Cantidad
            };

            _lineaResumenRepositorio.Agregar(LineaResumen);
            _lineaResumenRepositorio.Save();
        }

        public void Delete(long Id)
        {
            throw new NotImplementedException();
        }

        public string DeterminarTipoDeLista(List<LineaResumenDto> lista)
        {
            var listaPaga = new List<LineaResumenDto>();


            foreach (var item in lista)
            {
                var lineaResumen = new LineaResumenDto()
                {
                    Precio = item.Precio,
                    Cantidad = item.Cantidad,
                    EntradaId = item.EntradaId,
                    Subtotal = item.Subtotal
                };

                if (lineaResumen.Precio > 0m)
                {
                    listaPaga.Add(lineaResumen);
                }
            }

            return listaPaga.Any() ? "ListaPaga" : "ListaGratuita";
        }

        public IEnumerable<LineaResumenDto> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LineaResumenDto> GetByFilter(string cadenaBuscar)
        {
            throw new NotImplementedException();
        }

        public LineaResumenDto GetById(long? Id)
        {
            var LienaDetalle = _lineaResumenRepositorio.ObtenerPorId(Id, "Entrada, Resumen");

            return new LineaResumenDto()
            {
                Id = LienaDetalle.Id,
                Cantidad = LienaDetalle.Cantidad
            };
        }

        public long ObtenerSiguienteId()
        {
            var lista = _lineaResumenRepositorio.ObtenerTodo();

            return lista.Any() ? lista.Max(x => x.Id) : 1;
        }

        public int SumarCantidadesVendidas(long entradaId)
        {
            var Lista = _lineaResumenRepositorio.ObtenerTodo().Where(x => x.EntradaId == entradaId);

            var numero = 0;

            foreach (var item in Lista)
            {
                numero = numero + item.Cantidad;
            }

            return numero;
        }

        public void Update(LineaResumenDto Dto)
        {
            throw new NotImplementedException();
        }
    }
}
