﻿using Dominio.Repositorio.Provincia;
using IServicio.Provincia;
using IServicio.Provincia.Dto;
using Servicio.Base;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.Provincia
{
    public class ProvinciaServicio : ServicioBase, IProvinciaServicio
    {
        private readonly IProvinciaRepositorio _provinciaRepositorio;

        public ProvinciaServicio(IProvinciaRepositorio provinciaRepositorio)
        {
            _provinciaRepositorio = provinciaRepositorio;
        }

        public void Add(ProvinciaDto Dto)
        {
            var provincia = new Dominio.Entidades.Provincia
            {
                Nombre = Dto.Nombre
            };

            _provinciaRepositorio.Agregar(provincia);
            _provinciaRepositorio.Save();
        }

        public void Delete(long Id)
        {
            _provinciaRepositorio.Eliminar(Id);

            _provinciaRepositorio.Save();
        }

        public IEnumerable<ProvinciaDto> GetByFilter(string cadenaBuscar)
        {
            return _provinciaRepositorio.ObtenerPorFlitro(x => x.Nombre.Contains(cadenaBuscar))
                .Select(x => new ProvinciaDto()
                {
                    Id = x.Id,
                    Nombre = x.Nombre
                }).ToList();
        }

        public ProvinciaDto GetById(long? Id)
        {
            var provincia = _provinciaRepositorio.ObtenerPorId(Id);

            return new ProvinciaDto()
            {
                Id = provincia.Id,
                Nombre = provincia.Nombre
            };
        }

        public void Update(ProvinciaDto Dto)
        {
            var provincia = _provinciaRepositorio.ObtenerPorId(Dto.Id);

            provincia.Nombre = Dto.Nombre;

            _provinciaRepositorio.Modificar(provincia);
            _provinciaRepositorio.Save();
        }
    }
}
