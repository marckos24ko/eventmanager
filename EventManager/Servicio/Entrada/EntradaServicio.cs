﻿using Dominio.Repositorio.Entrada;
using Dominio.Repositorio.Evento;
using IServicio.Entrada;
using IServicio.Entrada.Dto;
using Servicio.Base;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.Entrada
{
    public class EntradaServicio : ServicioBase, IEntradaServicio
    {
        private readonly IEntradaRepositorio _entradaRepositorio;
        private readonly IEventoRepositorio _eventoRepositorio;

        public EntradaServicio(IEntradaRepositorio entradaRepositorio)
        {
            _entradaRepositorio = entradaRepositorio;
        }

        public void Add(EntradaDto Dto)
        {
            var entrada = new Dominio.Entidades.Entrada()
            {
                Nombre = Dto.Nombre,
                Descripcion = Dto.Descripcion,
                Cantidad = Dto.Cantidad,
                Precio = Dto.Precio,
                TipoEntrada = Dto.TipoEntrada,
                FechaInicioVenta = Dto.FechaInicioVenta,
                FechaFinVenta = Dto.FechaFinVenta,
                CantidadMaximaPorCompra = Dto.CantidadMaximaPorCompra,
                CantidadVendida = 0,
                CanalDeVentas = Dto.CanalDeVentas,
                EventoId = Dto.EventoId
            };

            _entradaRepositorio.Agregar(entrada);
            _entradaRepositorio.Save();
        }

        public void Delete(long Id)
        {
            _entradaRepositorio.Eliminar(Id);
            _entradaRepositorio.Save();
        }

        public IEnumerable<EntradaDto> GetByFilter(string cadenaBuscar)
        {
            return _entradaRepositorio.ObtenerPorFlitro(x => x.Nombre.Contains(cadenaBuscar), "Evento")
                 .Select(x => new EntradaDto()
                 {
                     Id = x.Id,
                     Nombre = x.Nombre,
                     Descripcion = x.Descripcion,
                     Cantidad = x.Cantidad,
                     Precio = x.Precio,
                     TipoEntrada = x.TipoEntrada,
                     FechaInicioVenta = x.FechaInicioVenta,
                     FechaFinVenta = x.FechaFinVenta,
                     CantidadMaximaPorCompra = x.CantidadMaximaPorCompra,
                     CanalDeVentas = x.CanalDeVentas,
                     EventoId = x.EventoId,
                     EventoStr = x.Evento.Nombre

                 }).ToList();
        }

        public EntradaDto GetById(long? Id)
        {
            var entrada = _entradaRepositorio.ObtenerPorId(Id, "Evento");

            return new EntradaDto()
            {
                Id = entrada.Id,
                Nombre = entrada.Nombre,
                Descripcion = entrada.Descripcion,
                Cantidad = entrada.Cantidad,
                CantidadVendida = entrada.CantidadVendida,
                Precio = entrada.Precio,
                TipoEntrada = entrada.TipoEntrada,
                FechaInicioVenta = entrada.FechaInicioVenta,
                FechaFinVenta = entrada.FechaFinVenta,
                CantidadMaximaPorCompra = entrada.CantidadMaximaPorCompra,
                CanalDeVentas = entrada.CanalDeVentas,
                EventoId = entrada.EventoId,
                EventoStr = entrada.Evento.Nombre
            };
        }

        public IEnumerable<EntradaDto> GetByEventoId(long? eventoId)
        {

            var entrada = _entradaRepositorio.ObtenerTodo("Evento").Where(x => x.EventoId == eventoId);

            return entrada.Select(x => new EntradaDto()
            {
                Id = x.Id,
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                Cantidad = x.Cantidad,
                CantidadVendida = x.CantidadVendida,
                Precio = x.Precio,
                TipoEntrada = x.TipoEntrada,
                FechaInicioVenta = x.FechaInicioVenta,
                FechaFinVenta = x.FechaFinVenta,
                CantidadMaximaPorCompra = x.CantidadMaximaPorCompra,
                CanalDeVentas = x.CanalDeVentas,
                EventoId = x.EventoId,
                EventoStr = x.Evento.Nombre

            }).ToList();
        }

        public void Update(EntradaDto Dto)
        {
            var entrada = _entradaRepositorio.ObtenerPorId(Dto.Id, "Evento");

            entrada.Nombre = Dto.Nombre;
            entrada.Descripcion = Dto.Descripcion;
            entrada.Cantidad = Dto.Cantidad;
            entrada.Precio = Dto.Precio;
            entrada.TipoEntrada = Dto.TipoEntrada;
            entrada.FechaInicioVenta = Dto.FechaInicioVenta;
            entrada.FechaFinVenta = Dto.FechaFinVenta;
            entrada.CantidadMaximaPorCompra = Dto.CantidadMaximaPorCompra;
            entrada.CantidadVendida = Dto.CantidadVendida;
            entrada.CanalDeVentas = Dto.CanalDeVentas;

            _entradaRepositorio.Modificar(entrada);
            _entradaRepositorio.Save();
        }

        public IList<EntradaDto> GetByEventoIdd(long? eventoId)
        {
            var entrada = _entradaRepositorio.ObtenerTodo("Evento").Where(x => x.EventoId == eventoId);

            return entrada.Select(x => new EntradaDto()
            {
                Id = x.Id,
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                Cantidad = x.Cantidad,
                Precio = x.Precio,
                TipoEntrada = x.TipoEntrada,
                FechaInicioVenta = x.FechaInicioVenta,
                FechaFinVenta = x.FechaFinVenta,
                CantidadVendida = x.CantidadVendida,
                CantidadMaximaPorCompra = x.CantidadMaximaPorCompra,
                CanalDeVentas = x.CanalDeVentas,
                EventoId = x.EventoId,
                EventoStr = x.Evento.Nombre

            }).ToList();
        }

    }
}
