﻿using Dominio.Repositorio.Inscripcion;
using IServicio.Inscripcion;
using IServicio.Inscripcion.Dto;
using Servicio.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Servicio.Inscripcion
{
    public class InscripcionServicio : ServicioBase, IInscripcionServicio
    {
        private readonly IInscripcionRepositorio _inscripcionRepositorio;

        public InscripcionServicio(IInscripcionRepositorio inscripcionRepositorio)
        {
            _inscripcionRepositorio = inscripcionRepositorio;
        }

        public void Add(InscripcionDto Dto)
        {
            var inscripcion = new Dominio.Entidades.Inscripcion()
            {

                Id = ObtenerSiguienteId(),
                FechaInscripcion = Dto.Fecha,
                CodigoInscripcion = Dto.CodigoInscripcion,
                EventoId = Dto.EventoId,
                OyenteId = Dto.OyenteId
            };

            _inscripcionRepositorio.Agregar(inscripcion);
            _inscripcionRepositorio.Save();
        }

        public void Update(InscripcionDto Dto)
        {

        }

        public void Delete(long Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<InscripcionDto> GetByFilter(string cadenaBuscar)
        {
            throw new NotImplementedException();
        }

        public InscripcionDto GetById(long? Id)
        {
            var inscripcion = _inscripcionRepositorio.ObtenerPorId(Id);

            return new InscripcionDto()
            {
                Id = inscripcion.Id,
                CodigoInscripcion = inscripcion.CodigoInscripcion,
                Fecha = inscripcion.FechaInscripcion,
                OyenteId = inscripcion.OyenteId,
                EventoId = inscripcion.EventoId

            };
        }

        public IEnumerable<InscripcionDto> GetAll()
        {
            throw new NotImplementedException();
        }

        public long ObtenerSiguienteId()
        {
            var lista = _inscripcionRepositorio.ObtenerTodo();

            return lista.Any() ? lista.Max(x => x.Id) : 1;
        }

    }
}
