﻿using System.Collections.Generic;
using System.Linq;
using Dominio.Repositorio.TipoTarjeta;
using IServicio.TipoTarjeta;
using IServicio.TipoTarjeta.Dto;
using Servicio.Base;

namespace Servicio.TipoTarjeta
{
    public class TipoTarjetaServicio : ServicioBase, ITipoTarjetaServicio
    {
        private readonly ITipoTarjetaRepositorio _tipoTarjetaRepositorio;

        public TipoTarjetaServicio(ITipoTarjetaRepositorio tipoTarjetaRepositorio)
        {
            _tipoTarjetaRepositorio = tipoTarjetaRepositorio;
        }

        public IEnumerable<TipoTarjetaDto> GetAll()
        {
            var tarjetas = _tipoTarjetaRepositorio.ObtenerTodo();

            return tarjetas.Select(x => new TipoTarjetaDto()
            {
                Id = x.Id,
                Nombre = x.Nombre
            });
        }
    }
}
