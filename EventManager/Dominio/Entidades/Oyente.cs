﻿using Dominio.Entidades.MetaData;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("PersonaEntrada_Oyente")]
    [MetadataType(typeof(IOyente))]
    public class Oyente : PersonaEntrada
    {
        //Propiedades
        public bool Estado { get; set; }

        public string CodigoInscripcion { get; set; }

        public int NumeroTransaccion { get; set; }

    }
}
