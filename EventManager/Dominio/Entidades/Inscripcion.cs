﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Inscripcion")]
    [MetadataType(typeof(IInscripcion))]
    public class Inscripcion : EntidadBase
    {   
        public DateTime FechaInscripcion { get; set; }

        public string CodigoInscripcion { get; set; }

       // Propiedades de Navegacion

        public long EventoId { get; set; }

        public virtual Evento Evento { get; set; }

        public long OyenteId { get; set; }

        public virtual Oyente Oyente { get; set; }
    }
}
