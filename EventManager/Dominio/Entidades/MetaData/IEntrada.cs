﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dominio.Entidades.MetaData
{
    public interface IEntrada
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(40, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string Nombre { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(150, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string Descripcion { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Range(1, 999)]
        int Cantidad { get; set; }

        decimal Precio { get; set; }

        string TipoEntrada { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        DateTime? FechaInicioVenta { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        DateTime? FechaFinVenta { get; set; }

        [Range(1, 999)]
        int CantidadMaximaPorCompra { get; set; }

        int CantidadVendida { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        bool CanalDeVentas { get; set; }

    }
}
