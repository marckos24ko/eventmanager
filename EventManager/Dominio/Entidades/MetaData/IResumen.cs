﻿namespace Dominio.Entidades.MetaData
{
    public interface IResumen
    {
        decimal? Total { get; set; }
    }
}
