﻿using System.ComponentModel.DataAnnotations;

namespace Dominio.Entidades.MetaData
{

    public interface IPersonaEntrada
     {
         [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
         [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
         string Nombre { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
         [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
         string Apellido { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio")]
         [StringLength(13, ErrorMessage = "El campo {0} no debe superar los {1} caracteres")]
         string Celular { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(100, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string Direccion { get; set; }
    }
    
}
