﻿using System.ComponentModel.DataAnnotations;

namespace Dominio.Entidades.MetaData
{
    public interface ICategoria
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(100, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string Descripcion { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = @"Esta Activo?")]
        bool Estado { get; set; }
    }
}
