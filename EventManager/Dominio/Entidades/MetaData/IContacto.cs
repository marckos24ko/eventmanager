﻿using System.ComponentModel.DataAnnotations;

namespace Dominio.Entidades.MetaData
{

    public interface IContacto
    {
        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(40, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        [EmailAddress]
        string Mail { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(20, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        [Phone]
        string Telefono { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(100, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string RedSocial1 { get; set; }

        [StringLength(100, ErrorMessage = "El campo {0} no debe .superar los {1} caracteres.")]
        string RedSocial2 { get; set; }
    }
}
