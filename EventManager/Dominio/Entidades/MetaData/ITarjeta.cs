﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dominio.Entidades.MetaData
{
    public interface ITarjeta
    {

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        string TipoTarjeta { get; set; } //Enum

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        string NombreTitular { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        //[CreditCard]
        string Numero { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        int DniTitular { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Range(1, 9999)]
        int CodigoSeguridad { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        string FechaExpiracion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        string BancoEmisor { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        int Cuotas { get; set; } //Enum
    }
}
