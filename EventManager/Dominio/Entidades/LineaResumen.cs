﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Detalle_Resumen")]
    [MetadataType(typeof(ILineaResumen))]
    public class LineaResumen : EntidadBase
    {
        //Propiedades
        public string TipoEntrada { get; set; }

        public decimal? Precio { get; set; }

        public int Cantidad { get; set; }

        public decimal? Subtotal { get; set; }

        //Propiedades de Navegacion

        public long EntradaId { get; set; }

        public virtual Entrada Entrada { get; set; }

        public long? ResumenId { get; set; }

        public virtual Resumen Resumen { get; set; }

    }
}
