﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Resumen")]
    [MetadataType(typeof(IResumen))]
    public class Resumen : EntidadBase
    {
        //Propiedades

        public decimal? Total { get; set; }

        //Propiedades de Navegacion

        public virtual ICollection<LineaResumen> LineaResumenes { get; set; }

    }
}
