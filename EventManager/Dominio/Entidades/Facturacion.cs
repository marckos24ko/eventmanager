﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Facturacion")]
    [MetadataType(typeof(IFacturacion))]
    public class Facturacion : EntidadBase
    {
        //Propiedades

        public int CodPostal { get; set; }

        public string Direccion { get; set; }

        //Propiedades de Navegacion

        public long TarjetaId { get; set; }

        public virtual Tarjeta Tarjeta { get; set; }

        public long ProvinciaId { get; set; }

        public virtual Provincia Provincia { get; set; }

        public long CiudadId { get; set; }

        public virtual Ciudad Ciudad { get; set; }



    }
}
