﻿using Dominio.Entidades.MetaData;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Persona_Organizador")]
    [MetadataType(typeof(IOrganizador))]
    public class Organizador : Persona
    {
        //propiedades

        public int Legajo { get; set; }

        public bool Estado { get; set; }

        //propiedades de navegacion

        public virtual IEnumerable<Evento> Eventos { get; set; }
    }
}
