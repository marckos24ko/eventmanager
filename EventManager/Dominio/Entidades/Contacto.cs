﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Persona_Contacto")]
    [MetadataType(typeof(IContacto))]
    public class Contacto : EntidadBase
    {
        //Propiedades

        public string Mail { get; set; }

        public string Telefono { get; set; }

        public string RedSocial1 { get; set; }

        public string RedSocial2 { get; set; }


        //Propiedades de Navegacion

        public long OrganizadorId { get; set; }

        public virtual Organizador Organizador { get; set; }
    }
}
