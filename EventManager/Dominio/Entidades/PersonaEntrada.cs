﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("PersonaEntrada")]
    [MetadataType(typeof(IPersonaEntrada))]
    public class PersonaEntrada : EntidadBase
    {
        //Propiedades

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string Celular { get; set; }

        public string Direccion { get; set; }

        //Propiedades de Navegacion

        public long EventoId { get; set; }

        public virtual Evento Evento { get; set; }

        public long EntradaId { get; set; }

        public virtual Entrada Entrada { get; set; }

    }
}
