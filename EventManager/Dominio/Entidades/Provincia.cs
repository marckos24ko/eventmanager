﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Provincias")]
    [MetadataType(typeof(IProvincia))]
    public class Provincia : EntidadBase
    {
        //Propiedades

        public string Nombre { get; set; }

        //Propiedades de Navegacion

        public virtual ICollection<Ciudad> Ciudades { get; set; }

        public virtual ICollection<Persona> Personas { get; set; }

        public virtual IEnumerable<Evento> Eventos { get; set; }
    }
}
