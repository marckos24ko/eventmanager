﻿using DominioBase;

namespace Repositorio.Base
{
    public interface IRepositorio<T> : IRepositorioConsulta<T>, IRepositorioOperaciones<T> where T : EntidadBase
    {

    }
}
