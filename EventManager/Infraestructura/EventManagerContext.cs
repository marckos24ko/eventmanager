﻿using AppConexion;
using Dominio.Entidades;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Infraestructura
{
    public class EventManagerContext : DbContext
    {
        public EventManagerContext() : base(StringConexion.ObtenerCadenaConexion())
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EventManagerContext, Migrations.Configuration>());
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);
        }

        public IDbSet<Persona> Persona { get; set; }
        public IDbSet<Categoria_Evento> Categoria { get; set; }
        public IDbSet<Ciudad> Ciudad { get; set; }
        public IDbSet<Contacto> Contacto { get; set; }
        public IDbSet<Entrada> Entrada { get; set; }
        public IDbSet<Evento> Evento { get; set; }
        public IDbSet<Facturacion> Facturacion { get; set; }
        public IDbSet<Inscripcion> Inscripcion { get; set; }
        public IDbSet<LineaResumen> LineaResumen { get; set; }
        public IDbSet<Provincia> Provincia { get; set; }
        public IDbSet<Resumen> Resumen { get; set; }
        public IDbSet<Tarjeta> Tarjeta { get; set; }
        public IDbSet<PersonaEntrada> PersonaEntradas { get; set; }
        public IDbSet<Oyente> Oyentes { get; set; }
        public IDbSet<ListaBancos> ListaBancos { get; set; }
        public IDbSet<TipoTarjeta> TipoTarjeta { get; set; }
    }

}
