﻿using Infraestructura;
using IServicio.Entrada;
using IServicio.Entrada.Dto;
using IServicio.Evento;
using IServicio.LineaResumen;
using MVC.Models.Entrada;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class EntradasController : Controller
    {
        private EventManagerContext db = new EventManagerContext();

        private readonly IEntradaServicio _entradaServicio;
        private readonly IEventoServicio _eventoServicio;
        private readonly ILineaResumenServicio _lineaResumenServicio;

        public EntradasController()
        {

        }

        public EntradasController(IEntradaServicio entradaServicio, IEventoServicio eventoServicio, ILineaResumenServicio lineaResumenServicio)
        {
            _entradaServicio = entradaServicio;
            _eventoServicio = eventoServicio;
            _lineaResumenServicio = lineaResumenServicio;
        }

        // GET: Entradas/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var entrada = _entradaServicio.GetById(id);

            if (entrada == null)
            {
                return HttpNotFound();
            }
            return View(new EntradaVm()
            {
                Id = entrada.Id,
                Nombre = entrada.Nombre,
                Descripcion = entrada.Descripcion,
                FechaInicioVenta = entrada.FechaInicioVenta,
                FechaFinVenta = entrada.FechaFinVenta,
                Precio = entrada.Precio,
                Cantidad = entrada.Cantidad,
                CantidadMaximaPorCompra = entrada.CantidadMaximaPorCompra,
                CanalDeVentas = entrada.CanalDeVentas,
                TipoEntrada = entrada.TipoEntrada
            });
        }

        // GET: Entradas/Create
        [HttpGet]
        public ActionResult Create(long id)
        {
            var eventoId = id;

            List<SelectListItem> listaCanalVenta = new List<SelectListItem>()
            {
                new SelectListItem{Text = "Online" , Value = "Online"},
                new SelectListItem{Text = "Boleteria" , Value = "Boleteria"}
            };

            List<SelectListItem> listaTipoEntrada = new List<SelectListItem>()
            {
                new SelectListItem{Text = "Paga" , Value = "Paga"},
                new SelectListItem{Text = "Gratuita" , Value = "Gratuita"}
            };

            ViewBag.CanalventaId = listaCanalVenta;
            ViewBag.TipoEntradaId = listaTipoEntrada;

            return View(new EntradaVm()
            {
                EventoId = eventoId
            });
        }

        // POST: Entradas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EntradaVm entradaVm, long eventoId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var precio = entradaVm.Precio;

                    var entrada = ConvertirEntradaDto(entradaVm, precio, eventoId);

                    _entradaServicio.Add(entrada);
                }
            }

            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    ModelState.AddModelError(string.Empty, "No se pudo crear el registro por que ya existe");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }

                List<SelectListItem> listaCanalVenta = new List<SelectListItem>()
            {
                new SelectListItem{Text = "Online" , Value = "Online"},
                new SelectListItem{Text = "Boleteria" , Value = "Boleteria"}
            };

                List<SelectListItem> listaTipoEntrada = new List<SelectListItem>()
            {
                new SelectListItem{Text = "Paga" , Value = "Paga"},
                new SelectListItem{Text = "Gratuita" , Value = "Gratuita"}
            };

                ViewBag.CanalventaId = listaCanalVenta;
                ViewBag.TipoEntradaId = listaTipoEntrada;

                return View(entradaVm);
            }
            

            return RedirectToAction("AdministrarEvento", "Evento", new { id = eventoId });

        }

        // GET: Entradas/Edit/5
        public ActionResult Edit(long id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var entrada = _entradaServicio.GetById(id);

            if (entrada == null)
            {
                return HttpNotFound();
            }

            List<SelectListItem> listaCanalVenta = new List<SelectListItem>()
            {
                new SelectListItem{Text = "Online" , Value = "Online"},
                new SelectListItem{Text = "Boleteria" , Value = "Boleteria"}
            };

            List<SelectListItem> listaTipoEntrada = new List<SelectListItem>()
            {
                new SelectListItem{Text = "Paga" , Value = "Paga"},
                new SelectListItem{Text = "Gratuita" , Value = "Gratuita"}
            };

            ViewBag.CanalventaId = listaCanalVenta;
            ViewBag.TipoEntradaId = listaTipoEntrada;
            ViewData["id"] = entrada.EventoId;
            ViewBag.CantidadInicial = entrada.Cantidad;
            ViewBag.CantMaximaInicial = entrada.CantidadMaximaPorCompra;

            return View(new EntradaVm()
            {
                Nombre = entrada.Nombre,
                Descripcion = entrada.Descripcion,
                FechaInicioVenta = entrada.FechaInicioVenta,
                FechaFinVenta = entrada.FechaFinVenta,
                Precio = entrada.Precio,
                Cantidad = entrada.Cantidad,
                CantidadMaximaPorCompra = entrada.CantidadMaximaPorCompra,
                CanalDeVentas = entrada.CanalDeVentas,
                TipoEntrada = entrada.TipoEntrada,
                EventoId = entrada.EventoId

            });
        }

        // POST: Entradas/Edit/5
        [HttpPost]
        public ActionResult Edit(EntradaVm entradaVm, long eventoId)
        {
            var precios = entradaVm.Precio;

            Convert.ToDecimal(precios);

            try
            {
             
                if (ModelState.IsValid)
                {
                    var precio = precios;

                    var entrada = ConvertirEntradaDto(entradaVm, precio, eventoId);

                    _entradaServicio.Update(entrada);

                }

            }

            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    ModelState.AddModelError(string.Empty, "No se pudo crear el registro por que ya existe");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }

                List<SelectListItem> listaCanalVenta = new List<SelectListItem>()
            {
                new SelectListItem{Text = "Online" , Value = "Online"},
                new SelectListItem{Text = "Boleteria" , Value = "Boleteria"}
            };

                List<SelectListItem> listaTipoEntrada = new List<SelectListItem>()
            {
                new SelectListItem{Text = "Paga" , Value = "Paga"},
                new SelectListItem{Text = "Gratuita" , Value = "Gratuita"}
            };

                ViewBag.CanalventaId = listaCanalVenta;
                ViewBag.TipoEntradaId = listaTipoEntrada;

                return View(entradaVm);
            }

            return RedirectToAction("AdministrarEvento", "Evento", new { id = eventoId });
        }

        // GET: Entradas/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var entrada = _entradaServicio.GetById(id);

            if (entrada == null)
            {
                return HttpNotFound();
            }

            return View(new EntradaVm()
            {
                Id = entrada.Id,
                Nombre = entrada.Nombre,
                Descripcion = entrada.Descripcion,
                FechaInicioVenta = entrada.FechaInicioVenta,
                FechaFinVenta = entrada.FechaFinVenta,
                Precio = entrada.Precio,
                Cantidad = entrada.Cantidad,
                CantidadMaximaPorCompra = entrada.CantidadMaximaPorCompra,
                CanalDeVentas = entrada.CanalDeVentas,
                TipoEntrada = entrada.TipoEntrada,
                EventoId = entrada.EventoId
            });
        }

        // POST: Entradas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id, EntradaVm entradaVm)
        {
            var evento = _entradaServicio.GetById(id);

            try
            {
                _entradaServicio.Delete(id);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(string.Empty, "No se puede borrar, existen registros relacionados");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
                return View(entradaVm);
            }

            return RedirectToAction("AdministrarEvento", "Evento", new { id = evento.EventoId });
        }

        public ActionResult EntradasPorEvento(long id)
        {
            var entradas = _entradaServicio.GetByEventoIdd(id);

            ViewData["id"] = id;
            ViewBag.FechaActual = DateTime.Today;

            return View(entradas.Select(x => new EntradaVm()
            {
                Id = x.Id,
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                PrecioStr = x.Precio == 0m ? "Gratis" : "$" + x.Precio.ToString(),
                Precio = x.Precio,
                FechaInicioVenta = x.FechaInicioVenta,
                FechaFinVenta = x.FechaFinVenta,
                Cantidad = x.Cantidad,
                CantVendida = x.CantidadVendida,
                CantidadMaximaPorCompra = x.CantidadMaximaPorCompra <= (x.Cantidad - x.CantidadVendida) ? x.CantidadMaximaPorCompra : (x.Cantidad - x.CantidadVendida),
                EventoId = id
            }).ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EntradasPorEvento(IList<EntradaVm> modelItem, long id)
        {
            var eventoId = id;

            var ListaEntradas = new List<EntradaDto>();

            var entradas = _entradaServicio.GetByFilter(string.Empty);

            foreach (var item in modelItem)
            {
                var entrada = entradas.FirstOrDefault(x => x.Id == item.Id);
                var cantidadSeleccionada = item.Cantidad;

                var entrada2 = new EntradaDto
                {
                    Id = entrada.Id,
                    Nombre = entrada.Nombre,
                    Descripcion = entrada.Descripcion,
                    Cantidad = entrada.Cantidad,
                    CantSeleccionada = cantidadSeleccionada,
                    Precio = entrada.Precio,
                    TipoEntrada = entrada.TipoEntrada,
                    FechaInicioVenta = entrada.FechaInicioVenta,
                    FechaFinVenta = entrada.FechaFinVenta,
                    CantidadMaximaPorCompra = entrada.CantidadMaximaPorCompra,
                    CanalDeVentas = entrada.CanalDeVentas,
                    EventoId = entrada.EventoId
                };

                if (entrada2.CantSeleccionada > 0)
                {
                    ListaEntradas.Add(entrada2);
                }

            }

            TempData["lista"] = ListaEntradas;

            return RedirectToAction("Create", "Inscripcion", new { evento_Id = eventoId});

        }

        public ActionResult View(long id)
        {
            var entradas = _entradaServicio.GetByEventoIdd(id);

            ViewData["id"] = id;

            return View(entradas.Select(x => new EntradaVm()
            {
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                PrecioStr = x.Precio == null ? "Gratis" : "$" + x.Precio.ToString(),
                Precio = x.Precio,
                FechaFinVenta = x.FechaFinVenta,
                Cantidad = x.Cantidad,
                EventoId = id
            }).ToList());
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //Metodos Privados 

        //public ActionResult InscripcionEvento(long id, int cantidad, int total)
        //{

        //}

        private static EntradaDto ConvertirEntradaDto(EntradaVm entradaVm, decimal? precio, long eventoId)
        {
            return new EntradaDto()
            {
                Id = entradaVm.Id,
                Nombre = entradaVm.Nombre,
                Descripcion = entradaVm.Descripcion,
                TipoEntrada = entradaVm.TipoEntrada,
                Precio = precio,
                FechaInicioVenta = entradaVm.FechaInicioVenta,
                FechaFinVenta = entradaVm.FechaFinVenta,
                Cantidad = entradaVm.Cantidad,
                CantidadMaximaPorCompra = (precio > 0) ? entradaVm.CantidadMaximaPorCompra : 1,
                CantidadVendida = entradaVm.CantVendida,
                CanalDeVentas = entradaVm.CanalDeVentas,
                EventoId = eventoId

            };
        }


    }
}
