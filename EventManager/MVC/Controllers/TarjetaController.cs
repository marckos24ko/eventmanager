﻿using Infraestructura;
using IServicio.Tarjeta;
using IServicio.Tarjeta.Dto;
using System;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class TarjetaController : Controller
    {
        private EventManagerContext db = new EventManagerContext();

        private readonly ITarjetaServicio _tarjetaServicio;

        public TarjetaController()
        {

        }

        public TarjetaController(ITarjetaServicio tarjetaServicio)
        {
            _tarjetaServicio = tarjetaServicio;
        }

        // GET: Tarjeta/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Ciudad/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TarjetaDto tarjetaDto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var _tarjetaDto = tarjetaDto;

                    _tarjetaServicio.Add(_tarjetaDto);
                }

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}