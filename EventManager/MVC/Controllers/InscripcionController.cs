﻿using Infraestructura;
using IServicio.Ciudad;
using IServicio.Entrada;
using IServicio.Entrada.Dto;
using IServicio.Evento;
using IServicio.Facturacion;
using IServicio.Faturacion.Dto;
using IServicio.Inscripcion;
using IServicio.Inscripcion.Dto;
using IServicio.LineaResumen;
using IServicio.LineaResumen.Dto;
using IServicio.ListaBancos;
using IServicio.Oyente;
using IServicio.Oyente.Dto;
using IServicio.Resumen;
using IServicio.Resumen.Dto;
using IServicio.Tarjeta;
using IServicio.Tarjeta.Dto;
using IServicio.TipoTarjeta;
using MVC.Helpers.ComboBox.Ciudad;
using MVC.Helpers.ComboBox.Provincia;
using MVC.Models;
using MVC.Models.Comprador;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Linq;

namespace MVC.Controllers
{
    public class InscripcionController : Controller
    {
        private EventManagerContext db = new EventManagerContext();

        private readonly IInscripcionServicio _inscripcionServicio;
        private readonly IEventoServicio _eventoServicio;
        private readonly IEntradaServicio _entradaServicio;
        private readonly IResumenServicio _resumenServicio;
        private readonly ILineaResumenServicio _lineaResumenServicio;
        private readonly ITarjetaServicio _tarjetaServicio;
        private readonly IFacturacionServicio _facturacionServicio;
        private readonly IOyenteServicio _oyenteServicio;
        private readonly ICiudadServicio _ciudadServicio;
        private readonly IComboBoxProvincia _comboBoxProvincia;
        private readonly IComboBoxCiudad _comboBoxCiudad;
        private readonly IListaBancosServicio _listaBancosServicio;
        private readonly ITipoTarjetaServicio _tipoTarjetasServicio;



        public InscripcionController()
        {

        }

    
        public InscripcionController(IInscripcionServicio inscripcionServicio, IEventoServicio eventoServicio, IEntradaServicio entradaServicio, IResumenServicio resumenServicio, ILineaResumenServicio lineaResumenServicio, ITarjetaServicio tarjetaServicio, IFacturacionServicio facturacionServicio, IOyenteServicio oyenteServicio, IComboBoxCiudad comboBoxCiudad, IComboBoxProvincia comboBoxProvincia, ICiudadServicio ciudadServicio, ITipoTarjetaServicio tipoTarjetaServicio, IListaBancosServicio listaBancosServicio)
        {
            _inscripcionServicio = inscripcionServicio;
            _eventoServicio = eventoServicio;
            _entradaServicio = entradaServicio;
            _resumenServicio = resumenServicio;
            _lineaResumenServicio = lineaResumenServicio;
            _tarjetaServicio = tarjetaServicio;
            _facturacionServicio = facturacionServicio;
            _oyenteServicio = oyenteServicio;
            _ciudadServicio = ciudadServicio;
            _listaBancosServicio = listaBancosServicio;
            _tipoTarjetasServicio = tipoTarjetaServicio;
            _comboBoxProvincia = comboBoxProvincia;
            _comboBoxCiudad = comboBoxCiudad;
    }

        // GET: Inscripcion
        public ActionResult Index()
        {
            return View();
        }

        // GET: Inscripcion/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Inscripcion/Create
        [HttpGet]
        public ActionResult Create(long evento_Id)
        {
            var listaEntradas = TempData["lista"] as List<EntradaDto>;

            var evento  = _eventoServicio.GetById(evento_Id);

            var listaLineaResumen = new List<LineaResumenDto>();

            var listaLineaResumenGratuita = new List<LineaResumenDto>();

            var totalPagar = 0m;

            foreach (var item in listaEntradas)
            {
                var subtotal = item.Precio * item.CantSeleccionada;

                var lineaResumen = new LineaResumenDto()
                {
                    EntradaId = item.Id,
                    TipoEntrada = item.TipoEntrada,
                    Cantidad = item.CantSeleccionada,
                    Precio = item.Precio,
                    Subtotal = subtotal
                };

                totalPagar = totalPagar + (decimal)subtotal;

                listaLineaResumen.Add(lineaResumen);

                if (lineaResumen.Precio == 0m || lineaResumen.Precio == null)
                {
                    listaLineaResumenGratuita.Add(lineaResumen);
                }

            }

            if (_lineaResumenServicio.DeterminarTipoDeLista(listaLineaResumen) == "ListaPaga")
            {
                ViewBag.LineaResumenPago = JsonConvert.SerializeObject(listaLineaResumen);
                ViewBag.Titulo = "Comprador";
                ViewBag.Mensaje = "la factura de compra y ";
            }

            if (_lineaResumenServicio.DeterminarTipoDeLista(listaLineaResumen) == "ListaGratuita")
            {
                ViewBag.LineaResumenGratuito = JsonConvert.SerializeObject(listaLineaResumenGratuita);
                ViewBag.Titulo = "Datos de Usuario";
            }

            ViewBag.EventoId = evento_Id;
            ViewBag.TotalPagar = totalPagar;

            var trescuotas = totalPagar / 3;
            var seiscuotas = totalPagar / 6;
            var docecuotas = totalPagar / 12;

            List<SelectListItem> listaCuotas = new List<SelectListItem>()
            {
                new SelectListItem{Text = "1 " + "(" + ("$" + totalPagar.ToString("N2")) + ")", Value = "1"},
                new SelectListItem{Text = "3 " + "(" + ("$" + trescuotas.ToString("N2")) + ")", Value = "3"},
                new SelectListItem{Text = "6  " + "(" + ("$" + seiscuotas.ToString("N2")) + ")", Value = "6"},
                new SelectListItem{Text = "12  " + "(" + ("$" + docecuotas.ToString("N2")) + ")", Value = "12"}
            };

            List<SelectListItem> listaCuotaDebito = new List<SelectListItem>()
            {
                new SelectListItem{Text = "1 " + "(" + ("$" + totalPagar.ToString("N2")) + ")", Value = "1"}
            };

            List<SelectListItem> listaCiudad = new List<SelectListItem>()
            {
            };

            ViewBag.ListaBancos = _listaBancosServicio.GetAll();
            ViewBag.ListaTarjetas = _tipoTarjetasServicio.GetAll();
            ViewBag.ListaCuotas = listaCuotas;
            ViewBag.ListaCuotaDebito = listaCuotaDebito;
            ViewBag.ListaEntradas = JsonConvert.SerializeObject(listaEntradas);

            return View( new InscripcionViewModel()
            {
                Evento = evento,
                Entradas = listaEntradas,
                LineaResumenes = listaLineaResumen,
                Provincias = _comboBoxProvincia.Poblar(),
                Ciudades = listaCiudad
            });
        }

        // POST: Inscripcion/Create
        [HttpPost]
        public ActionResult Create(decimal? totalPagar, long eventoId, List<OyenteDto> listaOyentes, TarjetaDto _tarjeta, FacturacionDto _facturacion, List<LineaResumenDto> _lineaResumenes, List<EntradaDto> _listaEntradas, string _nombreComprador)
        {
            try
            {
                if (eventoId != null && listaOyentes.Count > 0)
                {
                    bool bandera = false;
                    var tarjeta = new TarjetaDto();
                    var tarjetaDto = new TarjetaDto();
                    var facturacionDto = new FacturacionDto();
                    var entradas = _entradaServicio.GetByEventoIdd(eventoId);
                    decimal? resumen = 0m;

                    if (_tarjeta.BancoEmisor != null)
                    {
                      tarjetaDto = _tarjeta;
                    }

                    if (_facturacion.Direccion != null)
                    {
                      facturacionDto = _facturacion;
                    }

                    if (totalPagar != 0m)
                    {
                      resumen = totalPagar;
                    }

                    var oyentes = listaOyentes;

                    var lineaResumenes = _lineaResumenes;

                    if (_tarjeta.BancoEmisor != null && _facturacion.Direccion != null && totalPagar != 0m)
                    {
                        var tarjetas = _tarjetaServicio.GetByFilter(string.Empty);

                        foreach (var item in tarjetas)
                        {
                            if (item.Numero == tarjetaDto.Numero)
                            {
                                bandera = true;

                                break;
                            }
                        }


                        if (!bandera)
                        {
                            _tarjetaServicio.Add(tarjetaDto);
                        }

                        tarjetas = _tarjetaServicio.GetByFilter(string.Empty);

                        foreach (var item in tarjetas)
                        {
                            if (item.Numero == tarjetaDto.Numero)
                            {
                                tarjeta = new TarjetaDto()
                                {
                                    Id = item.Id,
                                    NombreTitular = item.NombreTitular,
                                    BancoEmisor = item.BancoEmisor,
                                    CodigoSeguridad = item.CodigoSeguridad,
                                    Cuotas = item.Cuotas,
                                    DniTitular = item.DniTitular,
                                    FechaExpiracion = item.FechaExpiracion,
                                    Numero = item.Numero,
                                    TipoTarjeta = item.TipoTarjeta
                                };

                                break;
                            }
                        }


                        var facturacion = new FacturacionDto()
                        {
                            ProvinciaId = facturacionDto.ProvinciaId,
                            CiudadId = facturacionDto.CiudadId,
                            CodPostal = facturacionDto.CodPostal,
                            Direccion = facturacionDto.Direccion,
                            TarjetaId = tarjeta.Id
                        };

                        _facturacionServicio.Add(facturacion);

                    }

                    var resumenDto = new ResumenDto()
                    {
                        Total = resumen
                    };

                    _resumenServicio.Add(resumenDto);

                    foreach (var item in lineaResumenes)
                    {
                        long? resumenId = null;

                        resumenId = _resumenServicio.ObtenerUltimoResumen().Id;
                       

                        var lineaResumen = new LineaResumenDto()
                        {
                            EntradaId = item.EntradaId,
                            Cantidad = item.Cantidad,
                            Precio = item.Precio,
                            Subtotal = item.Subtotal,
                            TipoEntrada = item.TipoEntrada,
                            ResumenId = resumenId
                        };

                        _lineaResumenServicio.Add(lineaResumen);
                    }

                    foreach (var item in oyentes)
                    {
                        var oyente = new OyenteDto()
                        {
                            Apellido = item.Apellido,
                            Nombre = item.Nombre,
                            Celular = item.Celular,
                            Direccion = item.Direccion,
                            EventoId = eventoId,
                            EntradaId = item.EntradaId,
                            Estado = true
                        };

                        _oyenteServicio.Add(oyente);
                    }

                    var listaOyentes2 = new List<OyenteDto>();

                    foreach (var item in oyentes)
                    {
                        var oyente = new OyenteDto()
                        {
                            Id = item.Id,
                            Apellido = item.Apellido,
                            Nombre = item.Nombre,
                            Celular = item.Celular,
                            Direccion = item.Direccion,
                            EventoId = eventoId,
                            EntradaId = item.EntradaId,
                            Estado = true
                        };

                        listaOyentes2.Add(oyente);
                    }

                    var listaOyentes3 = new List<OyenteDto>();

                    foreach (var item in listaOyentes2)
                    {
                        var oyente = _oyenteServicio.ObtenerPorFiltroAvanzado(item.Nombre, item.Apellido, item.Celular, item.Direccion, item.EventoId, item.EntradaId);

                        if(oyente != null)
                        {
                            listaOyentes3.Add(oyente);
                        }

                    }

                    var listaOyentes4 = new List<OyenteDto>();

                    foreach (var item in listaOyentes3)
                    {
                        Random secuencia = new Random();

                        int a = secuencia.Next(0, 1000);
                        int b = secuencia.Next(1001, 2000);
                        int c = secuencia.Next(2001, 3000);

                        var numero = a.ToString() + "-" + b.ToString() + "-" + c.ToString();

                        var inscripcionDto = new InscripcionDto()
                        {
                            Fecha = DateTime.Now,
                            CodigoInscripcion = numero,
                            EventoId = eventoId,
                            OyenteId = item.Id

                        };

                        _inscripcionServicio.Add(inscripcionDto);

                        var oyente = _oyenteServicio.GetById(item.Id);

                        oyente.CodigoInscripcion = numero;

                        _oyenteServicio.Update(oyente);

                        oyente = _oyenteServicio.GetById(item.Id);

                        listaOyentes4.Add(oyente);
                    }

                    var TotalOyentes = _oyenteServicio.GetByFilter(string.Empty);

                    var numeroTransaccion = TotalOyentes.Max(x => x.NumeroTransaccion) + 1;

                    foreach (var oyente in listaOyentes4)
                    {

                        oyente.NumeroTransaccion = numeroTransaccion;

                        _oyenteServicio.Update(oyente);
                    }

                    foreach (var item in _listaEntradas)
                    {
                        var entrada = _entradaServicio.GetById(item.Id);

                        entrada.CantidadVendida = entrada.CantidadVendida + item.CantSeleccionada;

                        _entradaServicio.Update(entrada);
                    }

                }
            }

            catch(Exception ex)

            {
                var evento = _eventoServicio.GetById(eventoId);

                var listaLineaResumen = new List<LineaResumenDto>();

                var listaLineaResumenGratuita = new List<LineaResumenDto>();

                var _totalPagar = 0m;

                foreach (var item in _listaEntradas)
                {
                    var subtotal = item.Precio * item.CantSeleccionada;

                    var lineaResumen = new LineaResumenDto()
                    {
                        EntradaId = item.Id,
                        TipoEntrada = item.TipoEntrada,
                        Cantidad = item.CantSeleccionada,
                        Precio = item.Precio,
                        Subtotal = subtotal
                    };

                    _totalPagar = _totalPagar + (decimal)subtotal;

                    listaLineaResumen.Add(lineaResumen);

                    if (lineaResumen.Precio == 0m || lineaResumen.Precio == null)
                    {
                        listaLineaResumenGratuita.Add(lineaResumen);
                    }

                }

                if (_lineaResumenServicio.DeterminarTipoDeLista(listaLineaResumen) == "ListaPaga")
                {
                    ViewBag.LineaResumenPago = JsonConvert.SerializeObject(listaLineaResumen);
                }

                if (_lineaResumenServicio.DeterminarTipoDeLista(listaLineaResumen) == "ListaGratuita")
                {
                    ViewBag.LineaResumenGratuito = JsonConvert.SerializeObject(listaLineaResumenGratuita);
                }

                ViewBag.EventoId = eventoId;
                ViewBag.TotalPagar = totalPagar;

                var trescuotas = totalPagar / 3;
                var seiscuotas = totalPagar / 6;
                var docecuotas = totalPagar / 12;

                List<SelectListItem> listaCuotas = new List<SelectListItem>()
                {
                new SelectListItem{Text = "1 " + "(" + ("$" + totalPagar.ToString()) + ")", Value = "1"},
                new SelectListItem{Text = "3 " + "(" + ("$" + trescuotas.ToString()) + ")", Value = "3"},
                new SelectListItem{Text = "6  " + "(" + ("$" + seiscuotas.ToString()) + ")", Value = "6"},
                new SelectListItem{Text = "12  " + "(" + ("$" + docecuotas.ToString()) + ")", Value = "12"}
                };

                List<SelectListItem> listaCuotaDebito = new List<SelectListItem>()
                {
                new SelectListItem{Text = "1 " + "(" + ("$" + totalPagar.ToString()) + ")", Value = "1"}
                };

                List<SelectListItem> listaCiudad = new List<SelectListItem>()
                {
                };

                ViewBag.ListaBancos = _listaBancosServicio.GetAll();
                ViewBag.ListaTarjetas = _tipoTarjetasServicio.GetAll();
                ViewBag.ListaCuotas = listaCuotas;
                ViewBag.ListaCuotaDebito = listaCuotaDebito;
                ViewBag.ListaProvincias = _comboBoxProvincia.Poblar();

                return View(new InscripcionViewModel()
                {
                    Evento = evento,
                    Entradas = _listaEntradas,
                    LineaResumenes = listaLineaResumen,
                    Provincias = _comboBoxProvincia.Poblar(),
                    Ciudades = listaCiudad
                });

            }

            var lista = new List<Comprador>();

            var comprador = new Comprador() { Nombre = _nombreComprador};

            lista.Add(comprador);

            return Json(lista);

        }

        // GET: Inscripcion/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Inscripcion/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inscripcion/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Inscripcion/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private bool EnviarCorreo(string emailReceptor, string asunto, string mensaje, HttpPostedFileBase Fichero)
        {
            try
            {
                MailMessage correo = new MailMessage();
                correo.From = new MailAddress("eventmanageroficial@gmail.com");
                correo.To.Add(emailReceptor);
                correo.Subject = asunto;
                correo.Body = mensaje;
                correo.IsBodyHtml = true;
                correo.Priority = MailPriority.Normal;

                // se almacena los archivos adjuntos en una carpeta llamada "CorreosEnviados"

                string ruta = Server.MapPath("../CorreosEnviados");
                Fichero.SaveAs(ruta + "\\" + Fichero.FileName);

                Attachment adjunto = new Attachment(ruta + "\\" + Fichero.FileName);
                correo.Attachments.Add(adjunto);

                //configuracion del servidor smtp

                SmtpClient smtp = new SmtpClient
                {
                    Host = "smpt.gmail.com",
                    Port = 25,
                    EnableSsl = true,
                    UseDefaultCredentials = true
                };

                string sCuentaCorreo = "eventmanageroficial@gmail.com";
                string sPasswordCorreo = "38060619po";
                smtp.Credentials = new System.Net.NetworkCredential(sCuentaCorreo, sPasswordCorreo);

                smtp.Send(correo);

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public ActionResult CompraRealizada( string nombre)
        {
            ViewBag.Nombre = nombre;

            return View();
        }

        public ActionResult Ejemplo()
        {
            

            return View();
        }


        public JsonResult ObtenerCiudades(long provinciaId)
        {
            var ciudades = _ciudadServicio.GetAll(provinciaId);

            return Json(ciudades);

        }
    }
}
