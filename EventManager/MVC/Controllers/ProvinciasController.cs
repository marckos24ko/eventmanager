﻿using Infraestructura;
using IServicio.Ciudad;
using IServicio.Evento;
using IServicio.Provincia;
using IServicio.Provincia.Dto;
using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class ProvinciasController : Controller
    {
        private EventManagerContext db = new EventManagerContext();

        private readonly IProvinciaServicio _provinciaServicio;
        private readonly IEventoServicio _eventoServicio;
        private readonly ICiudadServicio _ciudadServicio;

        public ProvinciasController()
        {

        }

        public ProvinciasController(IProvinciaServicio provinciaServicio, IEventoServicio eventoServicio, ICiudadServicio ciudadServicio)
        {
            _provinciaServicio = provinciaServicio;
            _eventoServicio = eventoServicio;
            _ciudadServicio = ciudadServicio;
        }

        // GET: Provincias
        public ActionResult Index(string cadenaBuscar)
        {
            var provincias = _provinciaServicio.GetByFilter(!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty);
            var ciudades = _ciudadServicio.GetByFilter(string.Empty);

            var lista = new List<ProvinciaVm>();

            foreach (var item in provincias)
            {
                var eventos = _eventoServicio.GetAll().Where(x => x.OradorId == item.Id);

                if (eventos.Count() > 0)
                {
                    var provincia = new ProvinciaVm()
                    {
                        Id = item.Id,
                        Nombre = item.Nombre,
                        EventosAsociados = true,
                        CiudadesAsociadas = false
                    };

                    foreach (var propiedades in ciudades)
                    {
                       
                        if (propiedades.ProvinciaId == item.Id)
                        {
                            provincia.CiudadesAsociadas = true;

                        }
                       
                    }

                    lista.Add(provincia);

                }

                else
                {
                    var provincia = new ProvinciaVm()
                    {
                        Id = item.Id,
                        Nombre = item.Nombre,
                        EventosAsociados = false,
                        CiudadesAsociadas = false
                    };

                    foreach (var propiedades in ciudades)
                    {

                        if (propiedades.ProvinciaId == item.Id)
                        {
                            provincia.CiudadesAsociadas = true;
                        }

                    }

                    lista.Add(provincia);
                }
            }

            return View(lista.Select(x => new ProvinciaVm()
            {
                Id = x.Id,
                Nombre = x.Nombre,
                EventosAsociados = x.EventosAsociados,
                CiudadesAsociadas = x.CiudadesAsociadas

            }).ToList());
        }

        // GET: Provincias/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var provincia = _provinciaServicio.GetById(id);

            if (provincia == null)
            {
                return HttpNotFound();
            }

            return View(new ProvinciaVm()
            {
                Id = provincia.Id,
                Nombre = provincia.Nombre
            });
        }

        // GET: Provincias/Create
        //ñamñamñam
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Provincias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre")] ProvinciaVm provinciaVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var provincia = CargarProvinciaDto(provinciaVm);

                    _provinciaServicio.Add(provincia);

                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    ModelState.AddModelError(string.Empty, "No se pudo crear el registro por que ya existe");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            return RedirectToAction("Index");
        }

        // GET: Provincias/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var provincia = _provinciaServicio.GetById(id);

            if (provincia == null)
            {
                return HttpNotFound();
            }

            return View(new ProvinciaVm()
            {
                Nombre = provincia.Nombre
            });
        }

        // POST: Provincias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] ProvinciaVm provinciaVm)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var provinciaDto = CargarProvinciaDto(provinciaVm);

                    _provinciaServicio.Update(provinciaDto);

                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    ModelState.AddModelError(string.Empty, "No se pudo crear el registro por que ya existe");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }

            return RedirectToAction("Index");
        }

        // GET: Provincias/Delete/5
        public ActionResult Delete(long? id, ProvinciaVm ProvinciaVm)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var provincia = _provinciaServicio.GetById(id);

            if (provincia == null)
            {
                return HttpNotFound();
            }

            return View(new ProvinciaVm()
            {
                Nombre = provincia.Nombre
            });
        }

        // POST: Provincias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id, ProvinciaVm provinciaVm)
        {
            try
            {

                _provinciaServicio.Delete(id);


            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(string.Empty, "No se puede borrar, existen registros relacionados");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            //return View(provinciaVm);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //metodos privados 

        private static ProvinciaDto CargarProvinciaDto(ProvinciaVm provinciaVm)
        {
            return new ProvinciaDto()
            {
                Id = provinciaVm.Id,
                Nombre = provinciaVm.Nombre
            };
        }
    }
}
