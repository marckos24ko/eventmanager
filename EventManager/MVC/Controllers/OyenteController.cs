﻿using IServicio.Entrada;
using IServicio.Evento;
using IServicio.Inscripcion;
using IServicio.Oyente;
using IServicio.Oyente.Dto;
using RazorPDF;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class OyenteController : Controller

    {
        private readonly IOyenteServicio _oyenteServicio;
        private readonly IEntradaServicio _entradaServicio;
        private readonly IEventoServicio _eventoServicio;
        private readonly IInscripcionServicio _inscripcionServicio;

        public OyenteController()
        {

        }

        public OyenteController(IOyenteServicio oyenteServicio, IEntradaServicio entradaServicio, IEventoServicio eventoServicio, IInscripcionServicio inscripcionServicio)
        {
            _oyenteServicio = oyenteServicio;
            _entradaServicio = entradaServicio;
            _eventoServicio = eventoServicio;
            _inscripcionServicio = inscripcionServicio;
        }

        // GET: Oyente
        public ActionResult GenerarEntradas()
        {
            var _oyentes = _oyenteServicio.GetByFilter(string.Empty);

            var numeroTransaccion = _oyentes.Max(x => x.NumeroTransaccion);

            var lista = _oyenteServicio.ObtenerOyentesUltimaVenta(numeroTransaccion);

            var listaOyentes = new List<OyenteDto>();

            foreach (var item in lista)
            {
                var oyente =  _oyenteServicio.ObtenerPorFiltroAvanzado(item.Nombre, item.Apellido, item.Celular, item.Direccion, item.EventoId, item.EntradaId);

                if (oyente != null)
                {
                    var entrada = _entradaServicio.GetById(item.EntradaId);
                    var evento = _eventoServicio.GetById(item.EventoId);

                    var _oyente = new OyenteDto()
                    {
                        Nombre = oyente.Nombre,
                        Apellido = oyente.Apellido,
                        Celular = oyente.Celular,
                        Direccion = oyente.Direccion,
                        EventoStr = evento.Nombre,
                        EntradaStr = entrada.Nombre,
                        CodigoInscripcion = oyente.CodigoInscripcion,
                        Evento = evento,
                        Entrada = entrada
                    };

                    listaOyentes.Add(_oyente);
                }
            }

            return View(listaOyentes.Select(x => new OyenteDto()
            {
                Nombre = x.Nombre,
                Apellido = x.Apellido,
                Celular = x.Celular,
                Direccion = x.Direccion,
                EventoStr = x.EventoStr,
                EntradaStr = x.EntradaStr,
                CodigoInscripcion = x.CodigoInscripcion,
                Evento = x.Evento,
                Entrada = x.Entrada

            }).ToList());

        }

        public ActionResult Imprimir()
        {
            return new ActionAsPdf("GenerarEntradas")
            {
                //FileName = "Entradas.pdf",
                IsJavaScriptDisabled = false,

                PageMargins = new Margins(10, 10, 10, 10),
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,

            };
        }

        public ActionResult Imprimir2()
        {
            var _oyentes = _oyenteServicio.GetByFilter(string.Empty);

            var numeroTransaccion = _oyentes.Max(x => x.NumeroTransaccion);

            var lista = _oyenteServicio.ObtenerOyentesUltimaVenta(numeroTransaccion);

            var listaOyentes = new List<OyenteDto>();

            foreach (var item in lista)
            {
                var oyente = _oyenteServicio.ObtenerPorFiltroAvanzado(item.Nombre, item.Apellido, item.Celular, item.Direccion, item.EventoId, item.EntradaId);

                if (oyente != null)
                {
                    var entrada = _entradaServicio.GetById(item.EntradaId);
                    var evento = _eventoServicio.GetById(item.EventoId);

                    var _oyente = new OyenteDto()
                    {
                        Nombre = oyente.Nombre,
                        Apellido = oyente.Apellido,
                        Celular = oyente.Celular,
                        Direccion = oyente.Direccion,
                        EventoStr = evento.Nombre,
                        EntradaStr = entrada.Nombre,
                        CodigoInscripcion = oyente.CodigoInscripcion,
                        Evento = evento,
                        Entrada = entrada
                    };

                    listaOyentes.Add(_oyente);
                }
            }

            return new ViewAsPdf("GenerarEntradas", listaOyentes);
        }

        public ActionResult Ejemplo()
        {
            return View();
        }
        // GET: Oyente/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Oyente/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Oyente/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Oyente/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Oyente/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Oyente/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Oyente/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
