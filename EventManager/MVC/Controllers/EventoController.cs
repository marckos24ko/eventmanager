﻿using Infraestructura;
using IServicio.Ciudad;
using IServicio.Entrada;
using IServicio.Evento;
using IServicio.Evento.Dto;
using IServicio.LineaResumen;
using IServicio.Orador;
using IServicio.Organizador;
using Microsoft.AspNet.Identity;
using MVC.Helpers;
using MVC.Helpers.ComboBox.Categoria;
using MVC.Helpers.ComboBox.Ciudad;
using MVC.Helpers.ComboBox.Orador;
using MVC.Helpers.ComboBox.Organizador;
using MVC.Helpers.ComboBox.Provincia;
using MVC.Models;
using MVC.Models.Entrada;
using MVC.Models.Evento;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Rotativa;
using System;
using System.Net.Http;
using Newtonsoft.Json;
using Dominio.Entidades;
using IServicio.Oyente;
using System.Threading.Tasks;

namespace MVC.Controllers
{
    public class EventoController : Controller
    {
        private readonly IEventoServicio _eventoServicio;
        private readonly IEntradaServicio _entradaServicio;
        private readonly IOradorServicio _oradorServicio;
        private readonly IOrganizadorServicio _organizadorServicio;
        private readonly ILineaResumenServicio _lineaResumenServicio;
        private readonly IComboBoxCategoria _comboBoxCategoria;
        private readonly IComboBoxOrganizador _comboBoxOrganizador;
        private readonly IComboBoxOrador _comboBoxOrador;
        private readonly IComboBoxProvincia _comboBoxProvincia;
        private readonly IComboBoxCiudad _comboBoxCiudad;
        private readonly ICiudadServicio _ciudadServicio;
        private readonly IOyenteServicio _oyenteServicio;

        private EventManagerContext db = new EventManagerContext();

        public EventoController()
        {

        }

        public EventoController(IEventoServicio eventoServicio, IEntradaServicio entradaServicio, IOradorServicio oradorServicio, IOrganizadorServicio organizadorServicio, ICiudadServicio ciudadServicio, ILineaResumenServicio lineaResumenServicio, IComboBoxCategoria comboBoxCategoria, IComboBoxOrganizador comboBoxOrganizador, IComboBoxOrador comboBoxOrador, IComboBoxProvincia comboBoxProvincia, IComboBoxCiudad comboBoxCiudad, IOyenteServicio oyenteServicio)
        {
            _eventoServicio = eventoServicio;
            _entradaServicio = entradaServicio;
            _oradorServicio = oradorServicio;
            _organizadorServicio = organizadorServicio;
            _ciudadServicio = ciudadServicio;
            _lineaResumenServicio = lineaResumenServicio;
            _oyenteServicio = oyenteServicio;
            _comboBoxCategoria = comboBoxCategoria;
            _comboBoxOrganizador = comboBoxOrganizador;
            _comboBoxOrador = comboBoxOrador;
            _comboBoxProvincia = comboBoxProvincia;
            _comboBoxCiudad = comboBoxCiudad;
        }

        // GET: Evento
        public ActionResult Index(string cadenaBuscar)
        {
            //var httpClient = new HttpClient()
            //{

            //    BaseAddress = new Uri("http://localhost:64171/")
            //};

            //var json = await httpClient.GetStringAsync("http://localhost:64171/api/Evento");
            //var lista = JsonConvert.DeserializeObject<List<EventoDto>>(json);
            //return View(lista);

            //var request = await clienteHttp.GetAsync("api/Evento");

            //if (request.IsSuccessStatusCode)
            //{
            //    var resultString = request.Content.ReadAsStringAsync().Result;
            //    var listado = JsonConvert.DeserializeObject<List<Evento>>(resultString);

            var listado = _eventoServicio.GetByFilter(!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty);

            return View(listado.Select(x => new EventoVm()
            {
                Id = x.Id,
                Categoria_EventoId = x.Categoria_EventoId,
                CategoriaStr = x.CategoriaStr,
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                Imagen = x.Imagen,
                FechaInicio = x.FechaInicio,
                FechaFin = x.FechaFin,
                HoraInicio = x.HoraInicio,
                HoraFin = x.HoraFin,
                Lugar = x.Lugar,
                Calle = x.Calle,
                Numero = x.Numero,
                Referencias = x.Referencias,
                OrganizadorId = x.OrganizadorId,
                OrganizadorStr = x.OrganizadorStr,
                OradorId = x.OradorId,
                OradorStr = x.OradorStr,
                ProvinciaId = x.ProvinciaId,
                ProvinciaStr = x.ProvinciaStr,
                CiudadId = x.CiudadId,
                CiudadStr = x.CiudadStr
            }).ToList());
        }


        public ActionResult MisEventos(string usuarioNombre)
        {
            ViewBag.userName = User.Identity.GetUserName();

            var evento = _eventoServicio.GetByUserName(usuarioNombre);

            var lista = new List<EventoVm>();

            foreach (var x in evento)
            {
                var eventoDto = new EventoVm()
                {
                    Id = x.Id,
                    Categoria_EventoId = x.Categoria_EventoId,
                    CategoriaStr = x.CategoriaStr,
                    Nombre = x.Nombre,
                    Descripcion = x.Descripcion,
                    Imagen = x.Imagen,
                    FechaInicio = x.FechaInicio,
                    FechaFin = x.FechaFin,
                    HoraInicio = x.HoraInicio,
                    HoraFin = x.HoraFin,
                    Lugar = x.Lugar,
                    Calle = x.Calle,
                    Numero = x.Numero,
                    Referencias = x.Referencias,
                    OrganizadorId = x.OrganizadorId,
                    OrganizadorStr = x.OrganizadorStr,
                    OradorId = x.OradorId,
                    OradorStr = x.OradorStr,
                    ProvinciaId = x.ProvinciaId,
                    ProvinciaStr = x.ProvinciaStr,
                    CiudadId = x.CiudadId,
                    CiudadStr = x.CiudadStr,
                    TieneEntradas = false
                };

                lista.Add(eventoDto);
            }

            foreach (var item in lista)
            {
                var entradas = _entradaServicio.GetByEventoId(item.Id);

                if (entradas.Count() > 0)
                {

                    item.TieneEntradas = true;

                }
            }
            
         

            return View(lista.Select(x => new EventoVm()
            {
                Id = x.Id,
                Categoria_EventoId = x.Categoria_EventoId,
                CategoriaStr = x.CategoriaStr,
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                Imagen = x.Imagen,
                FechaInicio = x.FechaInicio,
                FechaFin = x.FechaFin,
                HoraInicio = x.HoraInicio,
                HoraFin = x.HoraFin,
                Lugar = x.Lugar,
                Calle = x.Calle,
                Numero = x.Numero,
                Referencias = x.Referencias,
                OrganizadorId = x.OrganizadorId,
                OrganizadorStr = x.OrganizadorStr,
                OradorId = x.OradorId,
                OradorStr = x.OradorStr,
                ProvinciaId = x.ProvinciaId,
                ProvinciaStr = x.ProvinciaStr,
                CiudadId = x.CiudadId,
                CiudadStr = x.CiudadStr,
                TieneEntradas = x.TieneEntradas

            }).ToList());
        }

        // GET: Evento/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var evento = _eventoServicio.GetById(id);

            if (evento == null)
            {
                return HttpNotFound();
            }

            return View(new EventoVm()
            {
                Id = evento.Id,
                CategoriaStr = evento.CategoriaStr,
                Nombre = evento.Nombre,
                Descripcion = evento.Descripcion,
                Imagen = evento.Imagen,
                FechaInicio = evento.FechaInicio,
                FechaFin = evento.FechaFin,
                HoraInicio = evento.HoraInicio,
                HoraFin = evento.HoraFin,
                Lugar = evento.Lugar,
                Calle = evento.Calle,
                Numero = evento.Numero,
                Referencias = evento.Referencias,
                OrganizadorStr = evento.OrganizadorStr,
                OradorStr = evento.OradorStr,
                ProvinciaStr = evento.ProvinciaStr,
                CiudadStr = evento.CiudadStr
            });
        }

        public ActionResult DetalleEventoIndex(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var evento = _eventoServicio.GetById(id);
            var entradasCreadas = _entradaServicio.GetByEventoIdd(id).Count;
            var Entradas = _entradaServicio.GetByEventoIdd(id);
            var cantidadEntradas = 0;
            var fechaActual = DateTime.Today;


            if (evento == null)
            {
                return HttpNotFound();
            }

            foreach (var item in Entradas)
            {
                if (item.Cantidad > item.CantidadVendida)
                {
                    cantidadEntradas = cantidadEntradas + 1;
                }
            }

            if (entradasCreadas == 1)
            {
                foreach (var item in Entradas)
                {
                    if (fechaActual > item.FechaFinVenta)
                    {
                        var fechaFinalizada = item.FechaFinVenta;
                        ViewBag.FechaFinalizada = fechaFinalizada;
                    }
                }
            }

            ViewBag.EntradasCreadas = entradasCreadas;
            ViewBag.Entrada = Entradas;
            ViewBag.CantidadEntradas = cantidadEntradas;
            ViewBag.FechaActual = fechaActual.ToShortDateString();
            

            return View(new EventoVm()
            {
                Id = evento.Id,
                CategoriaStr = evento.CategoriaStr,
                Nombre = evento.Nombre,
                Descripcion = evento.Descripcion,
                Imagen = evento.Imagen,
                FechaInicio = evento.FechaInicio,
                FechaFin = evento.FechaFin,
                HoraInicio = evento.HoraInicio,
                HoraFin = evento.HoraFin,
                Lugar = evento.Lugar,
                Calle = evento.Calle,
                Numero = evento.Numero,
                Referencias = evento.Referencias,
                OrganizadorId = evento.OrganizadorId,
                OrganizadorStr = evento.OrganizadorStr,
                OradorStr = evento.OradorStr,
                ProvinciaStr = evento.ProvinciaStr,
                CiudadStr = evento.CiudadStr
            });
        }

        // GET: Evento/Create
        [HttpGet]
        public ActionResult Create()
        {
            List<SelectListItem> listaCiudad = new List<SelectListItem>()
                {
                };

            return View(new EventoABM()
            {
                Categorias = _comboBoxCategoria.Poblar(),
                Organizadores = _comboBoxOrganizador.Poblar(),
                Oradores = _comboBoxOrador.Poblar(),
                Provincias = _comboBoxProvincia.Poblar(),
                Ciudades = listaCiudad

            });
        }

        // POST: Evento/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EventoABM eventoAbm)
        {
            try
            {
                var pic = "No hay Imagen";
                var folder = "~/Content/Imagenes/Eventos";

                if (eventoAbm.BuscarImagen != null)
                {
                    pic = FileHelper.Upload(eventoAbm.BuscarImagen, folder);
                    pic = string.Format("{0}/{1}", folder, pic);
                }

                if (User.Identity.IsAuthenticated)
                {
                    var usuarioNombre = User.Identity.GetUserName();

                    var eventoDto = ConvertirEventoDto(eventoAbm, pic, usuarioNombre);

                    _eventoServicio.Add(eventoDto);
                }

            }

            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    ModelState.AddModelError(string.Empty, "No se pudo crear el registro por que ya existe");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }

                List<SelectListItem> listaCiudad = new List<SelectListItem>()
                {
                };

                return View(new EventoABM()
                {
                    Categorias = _comboBoxCategoria.Poblar(),
                    Organizadores = _comboBoxOrganizador.Poblar(),
                    Oradores = _comboBoxOrador.Poblar(),
                    Provincias = _comboBoxProvincia.Poblar(),
                    Ciudades = listaCiudad

                });
            }

            var UsuarioNombre = User.Identity.GetUserName();

            var evento = new EventoVm
            {
                Id = eventoAbm.Id,
                Nombre = eventoAbm.Nombre,
                FechaInicio = eventoAbm.FechaInicio,
                HoraInicio = eventoAbm.HoraInicio,
                UsuarioNombre = UsuarioNombre
            };

            return RedirectToAction("MisEventos", evento);
        }

        // GET: Evento/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var evento = _eventoServicio.GetById(id);

            if (evento == null)
            {
                return HttpNotFound();
            }

            return View(new EventoABM()
            {
                Categorias = _comboBoxCategoria.Poblar(),
                Organizadores = _comboBoxOrganizador.Poblar(),
                Oradores = _comboBoxOrador.Poblar(),
                Provincias = _comboBoxProvincia.Poblar(),
                Ciudades = _comboBoxCiudad.Poblar(),

                Categoria_EventoId = evento.Categoria_EventoId,
                Nombre = evento.Nombre,
                Descripcion = evento.Descripcion,
                FechaInicio = evento.FechaInicio,
                FechaFin = evento.FechaFin.Date,
                HoraInicio = evento.HoraInicio,
                HoraFin = evento.HoraFin,
                Lugar = evento.Lugar,
                Calle = evento.Calle,
                Numero = evento.Numero,
                Referencias = evento.Referencias,
                OrganizadorId = evento.OrganizadorId,
                OradorId = evento.OradorId,
                ProvinciaId = evento.ProvinciaId,
                CiudadId = evento.CiudadId
            });
        }

        // POST: Evento/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EventoABM eventoAbm)
        {
            try
            {

                var pic = "No hay Imagen";
                var folder = "~/Content/Imagenes/Eventos";

                if (eventoAbm.BuscarImagen != null)
                {
                    pic = FileHelper.Upload(eventoAbm.BuscarImagen, folder);
                    pic = string.Format("{0}/{1}", folder, pic);
                }

                if (User.Identity.IsAuthenticated)
                {
                    var usuarioId = User.Identity.GetUserId();

                    var eventoDto = ConvertirEventoDto(eventoAbm, pic, usuarioId);

                    _eventoServicio.Update(eventoDto);
                }


            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    ModelState.AddModelError(string.Empty, "No se pudo crear el registro por que ya existe");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
                return View(new EventoABM()
                {
                    Categorias = _comboBoxCategoria.Poblar(),
                    Organizadores = _comboBoxOrganizador.Poblar(),
                    Oradores = _comboBoxOrador.Poblar(),
                    Provincias = _comboBoxProvincia.Poblar(),
                    Ciudades = _comboBoxCiudad.Poblar()
                });
            }

            var UsuarioNombre = User.Identity.GetUserName();

            var evento = new EventoVm
            {
                Id = eventoAbm.Id,
                Nombre = eventoAbm.Nombre,
                FechaInicio = eventoAbm.FechaInicio,
                HoraInicio = eventoAbm.HoraInicio,
                UsuarioNombre = UsuarioNombre
            };

            return RedirectToAction("MisEventos", evento);
        }

        // GET: Evento/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var evento = _eventoServicio.GetById(id);

            if (evento == null)
            {
                return HttpNotFound();
            }

            return View(new EventoVm()
            {
                Id = evento.Id,
                CategoriaStr = evento.CategoriaStr,
                Nombre = evento.Nombre,
                Descripcion = evento.Descripcion,
                Imagen = evento.Imagen,
                FechaInicio = evento.FechaInicio,
                FechaFin = evento.FechaFin,
                HoraInicio = evento.HoraInicio,
                HoraFin = evento.HoraFin,
                Lugar = evento.Lugar,
                Calle = evento.Calle,
                Numero = evento.Numero,
                Referencias = evento.Referencias,
                OrganizadorStr = evento.OrganizadorStr,
                OradorStr = evento.OradorStr,
                ProvinciaStr = evento.ProvinciaStr,
                CiudadStr = evento.CiudadStr
            });
        }

        // POST: Evento/Delete/5
        [HttpPost]
        public ActionResult Delete(long id, EventoVm eventoVm)
        {
            try
            {

                _eventoServicio.Delete(id);

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(string.Empty, "No se puede borrar, existen registros relacionados");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }

                return View(eventoVm);
            }
            var UsuarioNombre = User.Identity.GetUserName();

            var evento = new EventoVm
            {
                Id = eventoVm.Id,
                Nombre = eventoVm.Nombre,
                FechaInicio = eventoVm.FechaInicio,
                HoraInicio = eventoVm.HoraInicio,
                UsuarioNombre = UsuarioNombre
            };

            return RedirectToAction("MisEventos", evento);
        }

        public ActionResult AdministrarEvento(long id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.EventoId = id;

            var evento = _eventoServicio.GetById(id);

            if (evento == null)
            {
                return HttpNotFound();
            }

            var entradas = _entradaServicio.GetByEventoId(id);

            var listaEntradas = new List<EntradaVm>();

            foreach (var item in entradas)
            {
                var entrada = item;

                var entradaVm = new EntradaVm
                {
                    Id = entrada.Id,
                    Nombre = entrada.Nombre,
                    Descripcion = entrada.Descripcion,
                    CantidadStr = entrada.CantidadVendida.ToString() + "/" + entrada.Cantidad.ToString(),
                    Precio = entrada.Precio,
                    TipoEntrada = entrada.TipoEntrada,
                    FechaInicioVenta = entrada.FechaInicioVenta,
                    FechaFinVenta = entrada.FechaFinVenta,
                    CantVendida = entrada.CantidadVendida,
                    CantidadMaximaPorCompra = entrada.CantidadMaximaPorCompra,
                    CanalDeVentas = entrada.CanalDeVentas,
                    PrecioStr = entrada.Precio != 0m ? "$ " + entrada.Precio.ToString() : "Gratuito"

                };

                listaEntradas.Add(entradaVm);
            }

            var orador = _oradorServicio.GetById(evento.OradorId);

            var oradorVm = new OradorVm()
            {
                Id = orador.Id,
                Apellido = orador.Apellido,
                Nombre = orador.Nombre,
                Celular = orador.Celular,
                Genero = orador.Genero,
                Telefono = orador.Telefono,
                FechaNacimiento = orador.FechaNacimiento,
                CiudadStr = orador.CiudadStr,
                ProvinciaStr = orador.ProvinciaStr,
                Cuil = orador.Cuil,
                Estado = orador.Estado
            };

            var organizador = _organizadorServicio.GetById(evento.OrganizadorId);

            var organizadorVm = new OrganizadorVm()
            {
                Id = organizador.Id,
                Legajo = organizador.Legajo,
                Apellido = organizador.Apellido,
                Nombre = organizador.Nombre,
                Celular = organizador.Celular,
                Genero = organizador.Genero,
                Telefono = organizador.Telefono,
                FechaNacimiento = organizador.FechaNacimiento,
                CiudadStr = organizador.CiudadStr,
                ProvinciaStr = organizador.ProvinciaStr,
                Cuil = organizador.Cuil,
                Estado = organizador.Estado
            };

            return View(new EventoVm()
            {
                Id = evento.Id,
                CategoriaStr = evento.CategoriaStr,
                Nombre = evento.Nombre,
                Descripcion = evento.Descripcion,
                Imagen = evento.Imagen,
                FechaInicio = evento.FechaInicio,
                FechaFin = evento.FechaFin,
                HoraInicio = evento.HoraInicio,
                HoraFin = evento.HoraFin,
                Lugar = evento.Lugar,
                Calle = evento.Calle,
                Numero = evento.Numero,
                Referencias = evento.Referencias,
                OrganizadorStr = evento.OrganizadorStr,
                OradorStr = evento.OradorStr,
                Orador = oradorVm,
                ProvinciaStr = evento.ProvinciaStr,
                CiudadStr = evento.CiudadStr,
                Entradas = listaEntradas,
                Organizador = organizadorVm
            });
        }

        public ActionResult InscripcionEvento(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var evento = _eventoServicio.GetById(id);

            if (evento == null)
            {
                return HttpNotFound();
            }

            return View(new EventoVm()
            {
                Id = evento.Id,
                CategoriaStr = evento.CategoriaStr,
                Nombre = evento.Nombre,
                Descripcion = evento.Descripcion,
                Imagen = evento.Imagen,
                FechaInicio = evento.FechaInicio,
                FechaFin = evento.FechaFin,
                HoraInicio = evento.HoraInicio,
                HoraFin = evento.HoraFin,
                Lugar = evento.Lugar,
                Calle = evento.Calle,
                Numero = evento.Numero,
                Referencias = evento.Referencias,
                OrganizadorStr = evento.OrganizadorStr,
                OradorStr = evento.OradorStr,
                ProvinciaStr = evento.ProvinciaStr,
                CiudadStr = evento.CiudadStr
            });

        }

        /// <summary>
        /// Reportes(PDF)
        /// </summary>
        /// <returns></returns>
        public ActionResult Reporte(string cadenaBuscar)
        {
            var evento = _eventoServicio.GetByFilter(!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty);

            return View(evento.Select(x => new EventoVm()
            {
                Id = x.Id,
                Categoria_EventoId = x.Categoria_EventoId,
                CategoriaStr = x.CategoriaStr,
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                Imagen = x.Imagen,
                FechaInicio = x.FechaInicio,
                FechaFin = x.FechaFin,
                HoraInicio = x.HoraInicio,
                HoraFin = x.HoraFin,
                Lugar = x.Lugar,
                Calle = x.Calle,
                Numero = x.Numero,
                Referencias = x.Referencias,
                OrganizadorId = x.OrganizadorId,
                OrganizadorStr = x.OrganizadorStr,
                OradorId = x.OradorId,
                OradorStr = x.OradorStr,
                ProvinciaId = x.ProvinciaId,
                ProvinciaStr = x.ProvinciaStr,
                CiudadId = x.CiudadId,
                CiudadStr = x.CiudadStr

            }).ToList());
        }

        public ActionResult Imprimir()
        {
            return new ActionAsPdf("Reporte") { FileName = "Prueba.pdf" };
        }
        //Metodos Privados

        private static EventoDto ConvertirEventoDto(EventoABM eventoAbm, string image, string usuarioNombre)
        {
            return new EventoDto()
            {
                Id = eventoAbm.Id,
                UsuarioNombre = usuarioNombre,
                Categoria_EventoId = eventoAbm.Categoria_EventoId,
                Nombre = eventoAbm.Nombre,
                Descripcion = eventoAbm.Descripcion,
                Imagen = !string.IsNullOrEmpty(image) ? image : "No hay Imagen",
                FechaInicio = eventoAbm.FechaInicio,
                FechaFin = eventoAbm.FechaFin,
                HoraInicio = eventoAbm.HoraInicio,
                HoraFin = eventoAbm.HoraFin,
                Lugar = eventoAbm.Lugar,
                Calle = eventoAbm.Calle,
                Numero = eventoAbm.Numero,
                Referencias = eventoAbm.Referencias,
                OrganizadorId = eventoAbm.OrganizadorId,
                OradorId = eventoAbm.OradorId,
                ProvinciaId = eventoAbm.ProvinciaId,
                CiudadId = eventoAbm.CiudadId
            };
        }

        public JsonResult ObtenerCiudades(long provinciaId)
        {
            var ciudades = _ciudadServicio.GetAll(provinciaId);

            return Json(ciudades);

        }
    }
}








