﻿using Dominio.Entidades;

namespace MVC.Models
{
    public class OyenteVm : PersonaEntradaVm
    {
        //Propiedades

        public bool Estado { get; set; }

        public string EventoStr { get; set; }

        public string EntradaStr { get; set; }

        public string CodigoEntrada { get; set; } // solo se usa para ticket




    }
}
