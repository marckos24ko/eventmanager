﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MVC.Models
{
    public class OradorVm : PersonaVm
    {
        //Propiedades

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(100, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public new string Nombre { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(100, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public new string Apellido { get; set; }

        [Display(Name = "Nombre")]
        public string ApyNom
        {
            get
            {
                return Nombre + " " + Apellido;
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(11, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Cuil { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaNacimiento { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        public string Genero { get; set; }

        [StringLength(11, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Telefono { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(10, ErrorMessage = "El campo {0} no debe superar los {1} caractere")]
        public string Celular { get; set; }

        public bool Estado { get; set; }

        [Display(Name = "Provincia")]
        public long ProvinciaId { get; set; }

        [Display(Name = "Provincia")]
        public string ProvinciaStr { get; set; }

        [Display(Name = "Ciudad")]
        public long CiudadId { get; set; }

        [Display(Name = "Ciudad")]
        public string CiudadStr { get; set; }

        public long? EventoId { get; set; } //se usa en evento asignarorador

        public string EstadoStr { get; set; }

        public bool EventosAsociados { get; set; }
    }
}
