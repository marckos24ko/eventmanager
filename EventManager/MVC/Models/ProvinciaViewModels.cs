﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Models
{
    public class ProvinciaVm :  EntidadBase
    {
        //Propiedades
        [Display(Name ="Nombre")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Index("Provincia_Nombre_Index", IsUnique = true)]
        public string Nombre { get; set; }

        public bool EventosAsociados { get; set; }

        public bool CiudadesAsociadas { get; set; }
    }
}
