﻿namespace MVC.Models
{

    public class Categoria_EventoVm : EntidadBase
    {
        //Propiedades

        public string Descripcion { get; set; }

        public bool Estado { get; set; }

        public string EstadoStr { get; set; }

        public bool EventosAsociados { get; set; }


    }
}
