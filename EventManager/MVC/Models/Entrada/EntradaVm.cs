﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MVC.Models.Entrada
{

    public class EntradaVm : EntidadBase
    {
        //Propiedades

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Nombre { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(150, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Range(1, 9999)]
        public int Cantidad { get; set; }

        public decimal? Precio { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Tipo de Entrada")]
        public string TipoEntrada { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha Inicio Venta")]
        public DateTime FechaInicioVenta { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha Fin Venta")]
        public DateTime FechaFinVenta { get; set; }

        [Display(Name = "Cantidad Max por Compra")]
        [Range(1, 9999)]
        public int CantidadMaximaPorCompra { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Canal de Venta")]
        public string CanalDeVentas { get; set; }

        [Display(Name = "Evento")]
        public long? EventoId { get; set; }

        [Display(Name = "Evento")]
        public string EventoStr { get; set; }

        [Display(Name = "Precio")]
        public string PrecioStr { get; set; }

        [Display(Name = "Cantidad")]
        public string CantidadStr { get; set; } // solo se usa para ver la cantidad de entradas disponilbes aun!!

        public int CantSeleccionada { get; set; }

        public int CantVendida { get; set; }

    }
}
