﻿using MVC.Models.Evento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class DetalleInscripcionViewModel : InscripcionViewModel
    {
        public DetalleInscripcionViewModel()
        {
            Evento = new EventoVm();
        }

        //Propiedades
        public int CodigoInscripcion { get; set; }

        //Propiedades de Navegacion
        public long EventoId { get; set; }

        public EventoVm Evento { get; set; }

        public long OyenteId { get; set; }

        public long ResumenId { get; set; }
    }
}