﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace MVC.Models
{

    public class PersonaVm : EntidadBase
    {
        //Propiedades

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Cuil { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime FechaNacimiento { get; set; }

        public string Genero { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.PhoneNumber)]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.PhoneNumber)]
        public string Celular { get; set; }

        public long ProvinciaId { get; set; }

        public long CiudadId { get; set; }

        public string ProvinciaStr { get; set; }

        public string CiudadStr { get; set; }

        public IEnumerable<SelectListItem> Provincias { get; set; }

        public IEnumerable<SelectListItem> Ciudades { get; set; }

        public IEnumerable<SelectListItem> Sexos { get; set; }

    }
}
