﻿using IServicio.Orador;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Orador
{
    public class ComboBoxOrador : IComboBoxOrador
    {
        private readonly IOradorServicio _oradorServicio;

        public ComboBoxOrador(IOradorServicio oradorServicio)
        {
            _oradorServicio = oradorServicio;
        }

        IEnumerable<SelectListItem> IComboBoxOrador.Poblar()
        {
            var orador = _oradorServicio.GetByFilter(string.Empty);

            return new SelectList(orador, "Id", "ApyNom");
        }
    }
}