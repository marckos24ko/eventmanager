﻿using IServicio.Ciudad;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Ciudad
{
    public class ComboBoxCiudad : IComboBoxCiudad
    {
        private readonly ICiudadServicio _ciudadServicio;

        public ComboBoxCiudad(ICiudadServicio ciudadServicio)
        {
            _ciudadServicio = ciudadServicio;
        }

        public IEnumerable<SelectListItem> Poblar()
        {
            var ciudad = _ciudadServicio.GetByFilter(string.Empty);

            return new SelectList(ciudad, "Id", "Nombre");
        }


    }
}