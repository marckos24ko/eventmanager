﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Ciudad
{
    public interface IComboBoxCiudad
    {
        IEnumerable<SelectListItem> Poblar();
    }
}