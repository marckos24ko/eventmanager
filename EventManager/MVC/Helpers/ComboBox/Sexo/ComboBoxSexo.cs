﻿using IServicio.Sexo;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Sexo
{
    public class ComboBoxSexo : IComboBoxSexo
    {
        private readonly ISexoServicio _sexoServicio;

        public ComboBoxSexo(ISexoServicio sexoServicio)
        {
            _sexoServicio = sexoServicio;
        }

        public IEnumerable<SelectListItem> Poblar()
        {
            var sexos = _sexoServicio.ObtenerSexo();

            return new SelectList(sexos, "Codigo", "Descripcion");
        }
    }

}