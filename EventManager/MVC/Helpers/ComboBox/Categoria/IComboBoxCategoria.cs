﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Categoria
{
    public interface IComboBoxCategoria
    {
        IEnumerable<SelectListItem> Poblar();
    }
}