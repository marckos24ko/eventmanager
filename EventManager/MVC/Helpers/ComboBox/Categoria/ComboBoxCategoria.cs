﻿using IServicio.Categoria_Evento;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Categoria
{
    public class ComboBoxCategoria : IComboBoxCategoria
    {
        private readonly ICategoria_EventoServicio _categoriaEventoServicio;

        public ComboBoxCategoria(ICategoria_EventoServicio categoria_EventoServicio)
        {
            _categoriaEventoServicio = categoria_EventoServicio;
        }

        IEnumerable<SelectListItem> IComboBoxCategoria.Poblar()
        {
            var categoria = _categoriaEventoServicio.GetByFilter(string.Empty);

            return new SelectList(categoria, "Id", "Descripcion");
        }
    }
}