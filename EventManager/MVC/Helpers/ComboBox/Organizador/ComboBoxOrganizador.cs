﻿using IServicio.Organizador;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Organizador
{
    public class ComboBoxOrganizador : IComboBoxOrganizador
    {
        private readonly IOrganizadorServicio _organizadorServicio;

        public ComboBoxOrganizador(IOrganizadorServicio organizadorServicio)
        {
            _organizadorServicio = organizadorServicio;
        }

        IEnumerable<SelectListItem> IComboBoxOrganizador.Poblar()
        {
            var organizador = _organizadorServicio.GetByFilter(string.Empty);

            return new SelectList(organizador, "Id", "ApyNom");
        }
    }
}