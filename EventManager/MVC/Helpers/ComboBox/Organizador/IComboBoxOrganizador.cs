﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Organizador
{
    public interface IComboBoxOrganizador
    {
        IEnumerable<SelectListItem> Poblar();
    }
}