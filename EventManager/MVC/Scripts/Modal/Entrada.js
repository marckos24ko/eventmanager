﻿$(".btnEntrada").click(function (eve) {
    $("#modal-content").load("/Entradas/EntradasPorEvento/" + $(this).data("id"));
});

$(".btnEntrada2").click(function (eve) {
    $("#modal-content").load("/Entradas/View/" + $(this).data("id"));
});

$(".btnNuevoEntrada").click(function (eve) {
    $("#modal-content").load("/Entradas/Create/" + $(this).data("id"));
});

$(".btnEditarEntrada").click(function (eve) {
    $("#modal-content").load("/Entradas/Edit/" + $(this).data("id"));
});

$(".btnDetalleEntrada").click(function (eve) {
    $("#modal-content").load("/Entradas/Details/" + $(this).data("id"));
});

$(".btnEliminarEntrada").click(function (eve) {
    $("#modal-content").load("/Entradas/Delete/" + $(this).data("id"));
});