﻿$("#btnNuevo").click(function (eve) {
    $("#modal-content").load("/Evento/Create" );
});

$(".btnEditar").click(function (eve) {
    $("#modal-content").load("/Evento/Edit/" + $(this).data("id"));
});

$(".btnDetalle").click(function (eve) {
    $("#modal-content").load("/Evento/Details/" + $(this).data("id"));
});

$(".btnEliminar").click(function (eve) {
    $("#modal-content").load("/Evento/Delete/" + $(this).data("id"));
});