﻿$(".btnCambiarOrganizador").click(function (eve) {
    $("#modal-content").load("/Organizador/AsignarOrganizador/" + $(this).data("id"));
});

$("#btnCrearOrganizador").click(function (eve) {
    $("#modal-content").load("/Organizador/Create" );
});

$(".btnEditarOrganizador").click(function (eve) {
    $("#modal-content").load("/Organizador/Edit/" + $(this).data("id"));
});

$(".btnDetalleOrganizador").click(function (eve) {
    $("#modal-content").load("/Organizador/Details/" + $(this).data("id"));
});

$(".btnEliminarOrganizador").click(function (eve) {
    $("#modal-content").load("/Organizador/Delete/" + $(this).data("id"));
});
