﻿$("#btnNuevo").click(function (eve) {
    $("#modal-content").load("/Provincias/Create");
});

$(".btnEditar").click(function (eve) {
    $("#modal-content").load("/Provincias/Edit/" + $(this).data("id"));
});

$(".btnDetalle").click(function (eve) {
    $("#modal-content").load("/Provincias/Details/" + $(this).data("id"));
});

$(".btnEliminar").click(function (eve) {
    $("#modal-content").load("/Provincias/Delete/" + $(this).data("id"));
});