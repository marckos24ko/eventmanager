﻿$("#btnNuevo").click(function (eve) {
    $("#modal-content").load("/Ciudad/Create");
});

$(".btnEditar").click(function (eve) {
    $("#modal-content").load("/Ciudad/Edit/" + $(this).data("id"));
});

$(".btnDetalle").click(function (eve) {
    $("#modal-content").load("/Ciudad/Details/" + $(this).data("id"));
});

$(".btnEliminar").click(function (eve) {
    $("#modal-content").load("/Ciudad/Delete/" + $(this).data("id"));
});