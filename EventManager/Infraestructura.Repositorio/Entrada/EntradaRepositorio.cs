﻿using Dominio.Repositorio.Entrada;
using Repositorio;

namespace Infraestructura.Repositorio.Entrada
{
    public class EntradaRepositorio : Repositorio<Dominio.Entidades.Entrada>, IEntradaRepositorio
    {

    }
}
