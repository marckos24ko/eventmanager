﻿using Servicio.Base.Dtos;

namespace IServicio.ListaBancos.Dto
{
    public class ListaBancosDto : DtoBase
    {
        public string Nombre { get; set; }
    }
}
