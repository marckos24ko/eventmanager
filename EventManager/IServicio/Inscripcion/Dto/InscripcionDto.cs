﻿using Servicio.Base.Dtos;
using System;

namespace IServicio.Inscripcion.Dto
{
    public class InscripcionDto : DtoBase
    {
        public DateTime Fecha{ get; set; }

        public string CodigoInscripcion { get; set; }

        public long EventoId { get; set; }

        public long OyenteId { get; set; }

    }
}
