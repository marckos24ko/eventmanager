﻿using IServicio.Entrada.Dto;
using System.Collections.Generic;

namespace IServicio.Entrada
{
    public interface IEntradaServicio
    {
        void Add(EntradaDto Dto);
        void Update(EntradaDto Dto);
        void Delete(long Id);

        IEnumerable<EntradaDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        IEnumerable<EntradaDto> GetByEventoId(long? eventoId);

        IList<EntradaDto> GetByEventoIdd(long? eventoId);

        EntradaDto GetById(long? Id);

        ///////////////////////////
        ///Metodos experimentale///
        ///////////////////////////

    }
}
