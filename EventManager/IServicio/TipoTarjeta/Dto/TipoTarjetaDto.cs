﻿using Servicio.Base.Dtos;

namespace IServicio.TipoTarjeta.Dto
{
    public class TipoTarjetaDto : DtoBase
    {
        public string Nombre { get; set; }
    }
}
