﻿using IServicio.Organizador.Dto;
using System.Collections.Generic;

namespace IServicio.Organizador
{
    public interface IOrganizadorServicio
    {
        void Add(OrganizadorDto Dto);
        void Update(OrganizadorDto Dto);
        void Delete(long Id);

        IEnumerable<OrganizadorDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        OrganizadorDto GetById(long? Id);

        int ObtenerSiguienteLegajo();
    }
}
