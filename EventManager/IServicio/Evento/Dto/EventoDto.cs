﻿using Servicio.Base.Dtos;
using System;
using System.ComponentModel.DataAnnotations;

namespace IServicio.Evento.Dto
{
    public class EventoDto : DtoBase
    {
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string Imagen { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime FechaInicio { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime FechaFin { get; set; }

        [DataType(DataType.Time)]
        public DateTime HoraInicio { get; set; }

        [DataType(DataType.Time)]
        public DateTime HoraFin { get; set; }

        public string Lugar { get; set; }

        public string Calle { get; set; }

        public int Numero { get; set; }

        public string Referencias { get; set; }

        public long OrganizadorId { get; set; }

        public string OrganizadorStr { get; set; }

        public long Categoria_EventoId { get; set; }

        public string CategoriaStr { get; set; }

        public long OradorId { get; set; }

        public string OradorStr { get; set; }

        public long ProvinciaId { get; set; }

        public string ProvinciaStr { get; set; }

        public long CiudadId { get; set; }

        public string CiudadStr { get; set; }

        public string UsuarioNombre { get; set; }

        public long? EntradaId { get; set; }
    }
}
