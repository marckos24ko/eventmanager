﻿using Servicio.Base.Dtos;

namespace IServicio.LineaResumen.Dto
{
    public class LineaResumenDto : DtoBase
    {
        public string TipoEntrada { get; set; }

        public decimal? Precio { get; set; }

        public int Cantidad { get; set; }

        public decimal? Subtotal { get; set; }

        public long EntradaId { get; set; }

        public string EntradaStr { get; set; }

        public long? ResumenId { get; set; }

    }
}
