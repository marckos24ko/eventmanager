﻿using IServicio.LineaResumen.Dto;
using System.Collections.Generic;

namespace IServicio.LineaResumen
{
    public interface ILineaResumenServicio
    {
        void Add(LineaResumenDto Dto);

        void Update(LineaResumenDto Dto);

        void Delete(long Id);

        IEnumerable<LineaResumenDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        LineaResumenDto GetById(long? Id);

        IEnumerable<LineaResumenDto> GetAll();

        long ObtenerSiguienteId();

        int SumarCantidadesVendidas(long entradaId);

       string DeterminarTipoDeLista(List<LineaResumenDto> lista); 

    }
}
