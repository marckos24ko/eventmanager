﻿using Servicio.Base.Dtos;
using System;

namespace IServicio.Persona.Dto
{
    public class PersonaDto : DtoBase
    {
        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string Cuil { get; set; }

        public DateTime FechaNacimiento { get; set; }

        public string Genero { get; set; }

        public string Telefono { get; set; }

        public string Celular { get; set; }

        public long ProvinciaId { get; set; }

        public long CiudadId { get; set; }

        public string ProvinciaStr { get; set; }

        public string CiudadStr { get; set; }
    }
}
