﻿using Servicio.Base.Dtos;

namespace IServicio.Ciudad.Dto
{
    public class CiudadDto : DtoBase
    {
        public string Nombre { get; set; }

        public int CP { get; set; }

        public long ProvinciaId { get; set; }

        public string ProvinciaStr { get; set; }
    }
}
