﻿using IServicio.Ciudad.Dto;
using System.Collections.Generic;

namespace IServicio.Ciudad
{
    public interface ICiudadServicio
    {
        void Add(CiudadDto Dto);
        void Update(CiudadDto Dto);
        void Delete(long Id);

        IEnumerable<CiudadDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        IEnumerable<CiudadDto> GetAll(long id);

        IEnumerable<CiudadDto> GetAll();

        CiudadDto GetById(long? Id);


    }
}
