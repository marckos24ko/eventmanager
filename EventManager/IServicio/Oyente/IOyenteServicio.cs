﻿using IServicio.Oyente.Dto;
using System.Collections.Generic;

namespace IServicio.Oyente
{
    public interface IOyenteServicio
    {
        void Add(OyenteDto Dto);
        void Update(OyenteDto Dto);
        void Delete(long Id);

        IEnumerable<OyenteDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        IEnumerable<OyenteDto> GetByEventoId(long id);

        OyenteDto GetById(long? Id);

        long ObtenerSiguienteId();

        OyenteDto ObtenerPorFiltroAvanzado(string nombre, string apellido, string Celular, string direccion, long eventoId, long entradaId);

        IEnumerable<OyenteDto> ObtenerOyentesUltimaVenta(int numeroTransaccion);
    }
}
