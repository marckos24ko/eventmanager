﻿using IServicio.Orador.Dto;
using System.Collections.Generic;

namespace IServicio.Orador
{
    public interface IOradorServicio
    {
        void Add(OradorDto Dto);
        void Update(OradorDto Dto);
        void Delete(long Id);

        IEnumerable<OradorDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        OradorDto GetById(long? Id);
    }
}
