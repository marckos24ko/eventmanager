﻿using IServicio.Resumen.Dto;
using System.Collections.Generic;

namespace IServicio.Resumen
{
    public interface IResumenServicio
    {
        void Add(ResumenDto Dto);

        void Update(ResumenDto Dto);

        void Delete(long Id);

        IEnumerable<ResumenDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        ResumenDto GetById(long? Id);

        ResumenDto ObtenerUltimoResumen();

    }
}
