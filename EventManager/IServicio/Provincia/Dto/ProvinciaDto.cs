﻿using Servicio.Base.Dtos;

namespace IServicio.Provincia.Dto
{
    public class ProvinciaDto : DtoBase
    {
        public string Nombre { get; set; }
    }
}
